
/**
 * Module dependencies.
 */

var express = require('express');
var http = require('http');
var mongoose = require('mongoose');
exports.db = mongoose.connect('localhost', 'libraries');
var library = require('./routes/library.js');
var path = require('path');
var socket = require('./routes/socketServer.js');

/*
 * SERVER
 */

var app = express();

app.configure(function(){
    app.set('port', process.env.PORT || 3000);
    app.set('views', __dirname + '/views');
    app.set('view engine', 'ejs');
    app.use(express.favicon(__dirname+"/public/favicon.ico", {maxAge: 2592000}));
    app.use(express.logger('dev'));
    app.use(express.bodyParser({uploadDir:'./public/media'}));
    app.use(express.methodOverride());
    app.use(express.cookieParser('your secret here'));
    app.use(express.session());
    app.use(app.router);
    app.use(require('stylus').middleware(__dirname + '/public'));
    app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('development', function(){
    app.use(express.errorHandler());
});

app.get('/', function(req, res) {res.sendfile("./public/index.html")});
app.get('/public/frame.html', function(req, res) { res.render('frame', {song:req.query.s}); });
app.get('/public/:id', function(req, res) {res.sendfile("./public/"+req.params.id)});

app.get('/public/css/:id', function(req, res) {res.sendfile("./public/css/"+req.params.id)});
app.get('/public/css/eggplant/:id', function(req, res) {res.sendfile("./public/css/eggplant/"+req.params.id)});
app.get('/public/css/eggplant/images/:id', function(req, res) {res.sendfile("./public/css/eggplant/images/"+req.params.id)});

app.get('/public/media/:id', function(req, res) {res.sendfile("./public/media/"+req.params.id)});

app.get('/public/js/:id', function(req, res) {res.sendfile("./public/js/"+req.params.id)});

app.get('/library', library.getDatabase);
app.del('/library', library.removeSong);

app.post('/media', library.upload);

app.post('/userPlaylist/', library.getFriendsPlaylistByOwner);
app.get('/userPlaylist/:id', library.getPlaylistByOwner);
app.get('/playlist/:id/:token', library.getPlaylistById);

app.post('/playlist/:id', library.createPlaylist);
app.put('/playlist/:id', library.editPlaylist);
app.del('/playlist/:id', library.removeSongFromPL);
app.del('/deleteplaylist/:id/:token', library.deletePlaylist);
app.put('/edit/', library.editSongID3);

var server = http.createServer(app).listen(app.get('port'), function(){
    console.log("Express server listening on port " + app.get('port'));
});

/*
 * SOCKET
 */

socket.init(server);


