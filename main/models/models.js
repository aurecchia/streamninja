var mongoose = require('mongoose');

var Schema = mongoose.Schema,
ObjectId = Schema.ObjectId;

function toLower (v) {
    return v.toLowerCase();
}

function toKewlCase(v){
    var split = v.split(/\W/);
    var res = [];
    for(var i = 0; i < split.length; i++){
        var caps = split[i].charAt(0).toUpperCase();
        res.push(caps + split[i].slice(1).toLowerCase());
    }
    return res.join(' ');
}

var User = new Schema({
    name: String, 
    rank: String
});

exports.User = User;

var Song = new Schema({
    title: String,
    artist: {type: String, set: toKewlCase},
    album: String,
    track: String,
    genre: String,
    path: String,
    duration: Number
});

exports.Song = Song;

var Album = new Schema({
    title: String,
    songs: [Song],
    artist: {type: String, set: toKewlCase},
});

exports.Album = Album;

var Artist = new Schema({
    name: {type: String, set: toKewlCase},
    albums: [Album]
});

exports.Artist = Artist;

var Library = new Schema({
    artists: [Artist]
});

exports.Library = Library;


var Playlist = new Schema({
    name: String,
    owner: String,
    type: String,
    songs: [Song]
});

exports.Playlist = Playlist;
