var activeFrame;
var inactiveFrame;

var playerStatus = new Object();

// player is global variable
player = new Object();
player.play = function(src, doNotBroadcast) {
    if (frame.loaded == false) { return; }
    if (src == undefined) {
        activeFrame.send({play:true, songBegin:Date.now()-playerUI.currentTime*1000});
        if (!doNotBroadcast) broadcast({play:true});
    } else {
        if (!doNotBroadcast) {
            broadcast({currentSong:src, data:$(".playing").data("data"), songBegin:Date.now()});
        }
        // check if src was already loaded in one of the frames. use it if possible.
        if (activeFrame.song == src) {
            // song was already loaded in activeFrame, use it
            activeFrame.send({currentTime:0, play:true});
        } else if (inactiveFrame.song == src) {
            // song was already loaded in inactiveFrame, use it
            swapFrames();
            inactiveFrame.send({play:false});
            activeFrame.send(playerStatus);
            activeFrame.send({currentTime:0, play:true});
        } else {
            swapFrames();
            activeFrame.setSource(src);
            // play will be handled on load
        }
    }
}
player.pause = function(doNotBroadcast) { 
    activeFrame.send({play:false}); 
    if (!doNotBroadcast) broadcast({play:false});
}
player.setCurrentTime = function(time, doNotBroadcast) { 
    console.log("player.setCurrentTime was called, this is not really good, please tell somebody about this");
    activeFrame.send({currentTime:time});
    if (!doNotBroadcast) broadcast({songBegin:now()-1000*time});
}
player.seeking = function(time, doNotBroadcast) {
    console.log("i dont think this should happen, tell somebody pls");
    activeFrame.send({currentTime:time, play:true});
    if (!doNotBroadcast) {
        broadcast({play:true, songBegin:Date.now(), time: time});
    }
}
player.loadNextSong = function(src, doNotBroadcast) {
    if (!doNotBroadcast) broadcast({nextSong:src});
    if (inactiveFrame.song != src) {
        inactiveFrame.setSource(src);
    }
}
player.send = function(msg, doNotBroadcast) { 
    activeFrame.send(msg);
    merge(playerStatus, msg);
    if (!doNotBroadcast) broadcast(msg);
}
var broadcast = function(msg) {
    // 1. check if we are in a social room and if we are master (with jquery, check if the master li is ourself).
    if ( ! $("#"+getId()).length > 0)
        return;
    if ( ! $("#"+getId()).hasClass("admin") )
        return;

    // 2. check if we have meaningful messages to send
    var tosend = new Object();
    tosend.id = getId();
    for (var key in msg) {
        if(key != "volume" && key != "currentTime") {
	    console.log("adding " + key);
            tosend[key] = msg[key];
        }
    }
    console.log("tosend.play="+tosend.play+" tosend.songBegin="+tosend.songBegin);
    if (tosend.play && !tosend.songBegin)
        tosend.songBegin == Date.now()-1000*playerUI.currentTime;

    console.log("broadcasting "+JSON.stringify(tosend));
    // 3. send messages
    if (tosend && !isEmpty(tosend)) {
        socketClient.send("player", tosend);
    }
}
player.getStatus = function() {
    var ret = JSON.parse(JSON.stringify(playerStatus));
    ret.currentSong = activeFrame.song;
    ret.nextSong = inactiveFrame.song;
    return ret;
}

// merges object 2 into obj1
var merge = function(obj1, obj2) {
    for (var key in obj2) {
        if (key != "play" && key != "currentTime") {
            if (typeof(obj2[key])!=="object") {
                // put in obj1 every non-object field of obj2
                obj1[key] = obj2[key];
            } else {
                if (obj1[key] == undefined) {
                    // put in obj1 every object field of obj2 which does not exist in obj1
                    obj1[key] = obj2[key];
                } else {
                    // or merge the inner object recursively
                    merge(obj1[key], obj2[key]);
                }
            }
        }
    }
}

var swapFrames = function() {
    var oldFrame = inactiveFrame;
    inactiveFrame = activeFrame;
    activeFrame = oldFrame;
}

window.addEventListener('load', function(e) {
    activeFrame = frame(document.getElementById("frame1"));
    inactiveFrame = frame(document.getElementById("frame2"));
}, false);

window.addEventListener('message', function(e) {
    if ((e.origin === "http://localhost:3000") || (e.origin === "http://streamninja.inf.usi.ch:3000")) {
        // console.log(e.data);
        var msg = JSON.parse(e.data);
        var source = (e.source == activeFrame.contentWindow) ? activeFrame : inactiveFrame ;
        if (msg.loaded !== undefined) {
            if (msg.loaded == true) {
                source.loaded = true;
                if (source == activeFrame) {
                    inactiveFrame.send({play:false});
                    activeFrame.send(playerStatus);
                    activeFrame.send({play:true});
                    broadcast({songBegin:Date.now()});
                }
            }
        }
        if (msg.ended !== undefined) {
            // song has ended, update playlist and play next song plzzz
            playerUI.play(findNextSong());
        }
        if (msg.timeupdate !== undefined) {
            playerUI.updateCurrentTime(parseInt(msg.timeupdate));
        }
        if (msg.songLength !== undefined) {
            if (source == activeFrame) {
                playerUI.updateTotalTime(parseInt(msg.songLength));
                // console.log('updating time of '+activeFrame.song);
            }
        }
        if (msg.analyser !== undefined && source == activeFrame) {
            /*console.log("left="+msg.analyser.left+" right="+msg.analyser.right);*/
            playerUI.updateVisualizer(msg.analyser.left, msg.analyser.right);
        }
        if (msg.compressor !== undefined && source == activeFrame) {
            playerUI.updateCompReduction(msg.compressor.reduction);
        }
    }
}, false);


var frame = function(frame) {
    frame.loaded = false;
    frame.song = undefined;
    frame.send = function(msg) { 
        if (frame.loaded == true) {
            frame.contentWindow.postMessage(JSON.stringify(msg), "http://streamninja.inf.usi.ch:3000"); 
        }
    }
    frame.setSource = function(src) {
        frame.song = src;
        frame.loaded = false;
        frame.src = "/public/frame.html?s=media/"+src;
    }
    return frame;
}
