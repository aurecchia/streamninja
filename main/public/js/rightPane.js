
// rightPane is a global variable.
// append to this object everything needed globally
// Do not use this object for internal functions, create local variables instead
rightPane = new Object();

// index of the next element in the list
var elementNumber = 1;

rightPane.actualPlaylist = new Object();
rightPane.actualPlaylist.name = "default"
rightPane.actualPlaylist.owner = undefined;

window.addEventListener('load', function(e) {

    rightPane.playlistDialog = function() {
        var dialog = $("<div id=\"newPlaylistDialog\"></div>")
        var dialogTitle = "New playlist";
        var dialogText = "Please enter the name for the new playlist.<br><br><span id=\"playlistNameErrors\"></span><br><label for=\"playlistName\">Playlist name </label><input id=\"playlistName\" type=\"text\" name=\"playlistName\" /> ";

        dialog.attr("title", dialogTitle);
        var dialogContent = $("<p></p>");
        dialogContent.append("<span class=\"ui-icon ui-icon-info\"</span>");
        dialogContent.append(dialogText);
        dialog.append(dialogContent);
        $("body").append(dialog);
        $("#playlistName").keyup(function(e) { if (e.keyCode === 13) { $("#newPlaylistDialog").next().find("button").first().trigger("click"); } });
        $("#newPlaylistDialog").dialog({
            resizable: false,
            modal: true,
            width: 500,
            buttons: {
                "Create room": function() {
                    if( $("#playlistName").attr("value").replace(/\s*/, "") === "" ) {
                        $("#playlistNameErrors").html("Not a valid playlist name!");
                        $("#playlistName").addClass("ui-state-error");
                    } else {
                        $("#playlistNameErrors").html("Valid!");
                        $("#playlistName").removeClass("ui-state-error");
                        rightPane.newPlaylist($("#playlistName").attr("value"));
                        $(this).dialog("close");
                    }
                },
                "Cancel": function() { $(this).dialog("close"); }
            },
            close: function() { $(this).remove(); }
        });
    }

    rightPane.newPlaylist = function(name) {
        if(name==undefined) {
            name=prompt("Please enter a name for the new playlist",""); 
        }

        //temp
        var type = "public";
	
        if (name!=null && type!=null) {
            var formData = new FormData();
            formData.append("token", getId());
	        formData.append("type", type);
            var xhr = new XMLHttpRequest();
            xhr.open('POST', '/playlist/'+name, true);
            xhr.onreadystatechange = function() {
                if(xhr.readyState == 4) {
                    if(xhr.status == 200) {
                        $("#activePlaylist").children("li").remove();
                        elementNumber = 1;
                        leftPane.displayUserPlaylist();
                        $("#activePlaylistTitle").text(name);
                        rightPane.actualPlaylist.name=name;
                        rightPane.actualPlaylist.owner = getId();
                    } else {
                        alert("Playlist alread exists");
                    }
                }
            }
            xhr.send(formData);
        }
    }

    rightPane.removeSong = function(title) {
        var xhr = new XMLHttpRequest();
        var formData = new FormData();
        formData.append("songName", JSON.stringify(title));
		formData.append("token", getId());
        xhr.open('DELETE', '/playlist/'+rightPane.actualPlaylist.name, true);
        xhr.send(formData);
    }
    
    var addSong = function(title)  {
        var xhr = new XMLHttpRequest();
        var formData = new FormData();
        formData.append("songName", JSON.stringify(title));
		formData.append("token", getId());
        xhr.open('PUT', '/playlist/'+rightPane.actualPlaylist.name, true);
        xhr.send(formData);
    }

    rightPane.editPlaylistTitle = function(newTitle) {
        var tmp = document.createElement("div");
        tmp.innerHTML = newTitle;
        var toSend = tmp.textContent || tmp.innerText;
        if (toSend.replace(/\s/,"") == "") {
            return;
        }
        var xhr = new XMLHttpRequest();
        var formData = new FormData();
        formData.append("newId", JSON.stringify(toSend));
		formData.append("token", getId());
        xhr.open('PUT', '/playlist/'+rightPane.actualPlaylist.name, true);
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4 && xhr.status == 200) {
                rightPane.actualPlaylist.name = toSend;
                leftPane.displayUserPlaylist();
            }
        }
        xhr.send(formData);
    }

    rightPane.clearAll= function() {
		titles = [];
		$("[id^=listItem_]").each(function() {titles.push(($(this).data("data").title))});
		rightPane.removeSong(titles);
		$("[id^=listItem_]").remove();
    }


    $("#rightPane").bind("drop", function(e) {
        var data=(JSON.parse( e.originalEvent.dataTransfer.getData('Data')));
        for(i = 0; i< data.length; i++) {
            var button = $("<div id=\"listButton_" + elementNumber +"\" class=\"playlistRemoveButton\">X</div>");
            button.bind("click", function() {rightPane.removeSong([$(this).parent().data("data").title]); $(this).parent().remove() });

            var title = $("<p>" + data[i].title + "</p>");

            var song = $("<li id=\"listItem_" + elementNumber + "\" draggable=\"true\"></li>");
            song.data("data", data[i]);
            song.bind("dblclick", function() { playerUI.play(this) });
            song.append(button);
            song.append(title);


            $("#activePlaylist").append(song);
	        addSong(data[i].title);
        }
    });

    
    
    //make the elements of the playlist sortable
    $("#activePlaylist").sortable();
}, false);
