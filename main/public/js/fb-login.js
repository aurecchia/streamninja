var id;
var name;
var friends;
var socket;

function getId() {
    return id;
}

function getName() {
    return name;
}

function getFriends() {
    return JSON.stringify(friends);
}

function getFriendById(id) {
    for (var friend in friends) {
        if (friends[friend].id == id) {
            return friends[friend];
        }
    }
    return undefined;
}

function login() {
    var paramsLocation=window.location.toString().indexOf('?');
    var params="";
    if (paramsLocation>=0)    params=window.location.toString().slice(paramsLocation);
    top.location = 'https://graph.facebook.com/oauth/authorize?client_id=386913884719650&scope=publish_stream&redirect_uri=http://streamninja.inf.usi.ch:3000'+params;
}

function logout() {
    FB.logout(function(response) {
        // console.log('User is now logged out');
	document.getElementById('fb-button').onclick=login;
	document.getElementById('fb-button').innerHTML='Login';	
	$("#newPlaylistButton").hide();
	id = undefined;
	leftPane.displayUserPlaylist();
    $("#fb-data").html("").addClass("hidden");
    $("#serverBox").addClass("hidden")
    $("#serverButtons").addClass("hidden")
    });
}

function updateServerInfo(response) {
///rly needed?
}

function updateBrowser(data) {

    $("#serverBox").removeClass("hidden")
    $("#serverButtons").removeClass("hidden")
    socketClient.connectToSocket();
    document.getElementById('fb-button').innerHTML='Logout ' + data.first_name;

    // enable playlist title editing
    $("#activePlaylistTitle").attr("contenteditable", "true");
    var tid;
    $("#activePlaylistTitle").bind("input", function() {
        clearTimeout(tid);
        tid = setTimeout(function() {
            rightPane.editPlaylistTitle($("#activePlaylistTitle").html())
        }, 1000);
    });
}


function getFriendsList(response, token, callback) {
    var xhr = new XMLHttpRequest();     
    xhr.open('GET','https://graph.facebook.com/'+response.id+'/friends?access_token='+token, true);
    xhr.onreadystatechange = function() {
     	if(xhr.readyState==4) {
     	    if(xhr.status == 200) {
		friends =JSON.parse(xhr.responseText).data;
		callback();
	    }
	}
    }
    xhr.send(null);
}

function fetchInfo() {
    // console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', function(response) {
	// console.log('Good to see you, ' + response.name + '.');
	var token = FB.getAuthResponse().accessToken;
	response.token = token;
	getFriendsList(response, token, function() {
	    id = response.id;
	    name=response.first_name;
	    leftPane.displayUserPlaylist();
	    leftPane.getPlaylist("default", getId());
	    rightPane.actualPlaylist.owner = getId();
	    updateBrowser(response);
	    updateServerInfo(response);
	});
    }); 
}

window.fbAsyncInit = function() {
    FB.init({
	appId      : '386913884719650', // App ID
	channelUrl : './channel.html', // Channel File
	status     : true, // check login status
	cookie     : true, // enable cookies to allow the server to access the session
	xfbml      : true  // parse XFBML
    });
    // Additional init code here
    FB.getLoginStatus(function(response) {
	if (response.status === 'connected') {
	    // console.log("connected");
	    //missing display of username
	    fetchInfo();
	    document.getElementById('fb-button').onclick=logout;
	    //return response.token;
	} else if (response.status === 'not_authorized') {
	    $("#newPlaylistButton").hide();
	    ///need to display authorization request
            // console.log("not_authorized");
        } else {
	    $("#newPlaylistButton").hide();
            // console.log("not_logged_in");
        }
    });
};

// Load the SDK Asynchronously
(function(d){
    var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement('script'); js.id = id; js.async = true;
    js.src = "//connect.facebook.net/en_US/all.js";
    ref.parentNode.insertBefore(js, ref);
}(document));
