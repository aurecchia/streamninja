$(function() { displayLibrary() });


displayLibrary = function() {
    var request = new XMLHttpRequest();
    request.open('GET', '/library', false);
    request.send();
    if (request.status === 200) {
        generateLibrary(JSON.parse(request.responseText));
    }
}

var generateLibrary = function generateLibrary(libraryData) {
    var code = $("<ul>\n</ul>");

    if (libraryData.length < 1) {
      return;
    }
    var artists = [];

    // generate a unique sorted list of artists from the objects received from the database
    for (index in libraryData) {
        for (artist in libraryData[index].artists) {
            artists.push(libraryData[index].artists[artist]);
        } 
    }
    artists.sort(function(a,b) { return (a.name.toUpperCase()).localeCompare(b.name.toUpperCase()); })

        for (artistIndex in artists) {
            var artist = artists[artistIndex];
            var artistElement = $("<li>\n<div class=\"artistBox\" draggable=\"true\">" + artist.name + "</div>\n</li>");
            var albumsList = $("<ul>\n</ul>\n");
            if (artist.name.length > 0) {
                var letter = artist.name.charAt(0).toUpperCase();
                var selector = "#letterSelector_" + letter;
                if (!$(selector).is(".enabled")) {
                    $(selector).addClass("enabled");
                    $(selector).bind("click", function() { search.filterByLetter($(this).html()) });
                }
            }

            artist.albums.sort(function(a,b) { return parseInt(a.year) - parseInt(b.year) == 0 ?  (a.title.toUpperCase()).localeCompare(b.title.toUpperCase()) : parseInt(a.year) - parseInt(b.year); })
            for (albumIndex in artist.albums) {
                var album = artist.albums[albumIndex];
                var albumElement = $("<li>\n<div class=\"albumBox\" draggable=\"true\">" + album.title + "</div>\n");
                var songsList = $("<ul>\n</ul>");


                album.songs.sort(function(a,b) { return (parseInt(a.track)) - (parseInt(b.track)); })
                for (songIndex in album.songs) {
                    var song = album.songs[songIndex];
                    var trackNumber = $("<span class=\"trackNumber\">" + ("0" + song.track).slice(-2) + "</span>");
                    var trackTitle = $("<span class=\"trackTitle\">" + song.title + "</span>");
                    var trackGenre = $("<span class=\"trackGenre\">" + song.genre + "</span>");
                    var length = ("0" + Math.floor(song.duration / 60)).slice(-2) + ":" + ("0" + (song.duration % 60)).slice(-2);
                    var trackLength = $("<span class=\"trackLength\">" + length + "</span>");

                    var songElement = $("<li class=\"trackBox\" draggable=\"true\">\n</li>\n");
                    songElement.append(trackNumber);
                    songElement.append(trackTitle);
                    songElement.append(trackLength);
                    songElement.append(trackGenre);
                    songElement.data("data", song);
                    songsList.append(songElement);
                }

                albumElement.children().after(songsList);
                albumsList.append(albumElement);
            }

            artistElement.children().after(albumsList);
            code.append(artistElement);
        }
    $("#library").html(code);

    $.contextMenu({
       selector: ".trackBox",
       items: {
        "rename": {name: "Rename", icon: "edit", callback: function(key, opt) { renameSong(opt.$trigger) } },
        "delete": {name: "Delete", icon: "delete", callback: function(key, opt) { deleteSong(opt.$trigger) } }
       }
    });

    // add listeners to hide albums or artists
    $(".artistBox, .albumBox").bind("click", function(event) {
        $(this).next("[style*=display: none]").children().show();
        $(this).next().slideToggle(300);
        $(this).next().children().show();
    });

    $(".trackBox").bind("dblclick", function(e) { playerUI.play(this); });
    
    // move stuff from the library to the playlist
    $(".artistBox").bind("dragstart", function(e) {
    	dragEvent = true;
    	var data = new Array();
    	$(this).next().find("li.trackBox").each(function() {data.push($(this).data("data"))});
    	e.originalEvent.dataTransfer.setData('Data', JSON.stringify(data));
    });
    
    $(".albumBox").bind("dragstart", function(e) {
    	dragEvent = true;
    	var data = new Array();
    	$(this).next().find("li").each(function() {data.push($(this).data("data"))});
    	e.originalEvent.dataTransfer.setData('Data', JSON.stringify(data));
    });

    $(".trackBox").bind("dragstart", function(e) {
    	dragEvent = true;
        // console.log(dragEvent);
    	var data = [$(this).data("data")];
    	e.originalEvent.dataTransfer.setData('Data', JSON.stringify(data));
    });

}


findNextSong = function() {
    var mode = $("#shuffleLoopButton").button("option", "icons").primary === "ui-icon-shuffle" ? "shuffle" : "loop";    
    // console.log(mode);
    if (mode === "loop") {
        return findNextLoopSong();
    } else {
        return findShuffleSong();
    }
}

findPrevSong = function() {
    var mode = $("#shuffleLoopButton").button("option", "icons").primary === "ui-icon-shuffle" ? "shuffle" : "loop";    

    if (mode === "loop") {
        return findPrevLoopSong();
    } else {
        return findShuffleSong();
    }
}

var findNextLoopSong = function() {
    if ($(".playing").parent().is("#activePlaylist")) {
        playing = $(".playing");
        if (!playing.is(":last-child")) {
            return playing.next();
        } else {
            return $("#activePlaylist li").first();
        }
    }
    var songs = $(".trackBox").toArray();
    for (i=0; i<songs.length; i++) {
        if ($(songs[i]).is(".playing")) {
            return songs[(i+1) % songs.length];
        }
    }
}

var findShuffleSong = function() {
    if ($(".playing").parent().is("#activePlaylist")) {
        var length = $("#activePlaylist li").length;
        if (length == 1) {
            return $(".playing");
        } else {
            var songs = $("#activePlaylist li").not(".playing").toArray();
            var index = Math.round(Math.random() * (songs.length - 1));
            return songs[index];
        }
    }
    var length = $(".trackBox").length;
    if (length == 1) {
        return $(".playing");
    }
    var songs = $(".trackBox").not(".playing").toArray();
    var index = Math.round(Math.random() * (songs.length - 1));
    return songs[index];
}

var findPrevLoopSong = function() {
    if ($(".playing").parent().is("#activePlaylist")) {
        playing = $(".playing");
        if (!playing.is(":first-child")) {
            return playing.prev();
        } else {
            return $("#activePlaylist li").last();
        }
    }
    var songs = $(".trackBox").toArray();
    for (i=0; i<songs.length; i++) {
        if ($(songs[i]).is(".playing")) {
            var index = (i - 1) >= 0 ? i - 1 : songs.length - 1;
            return songs[index];
        }
    }
}

var deleteSong = function(song) {
    var songName = $(song).data("data").title;
    var dialog = $("<div id=\"deleteDialog\"></div>")
    var dialogTitle = "Delete " + songName;
    var dialogText = "This song will be permanently removed.<br> Are you sure you want to remove " + songName + " from the server?";

    dialog.attr("title", dialogTitle);
    var dialogContent = $("<p></p>");
    dialogContent.append("<span class=\"ui-icon ui-icon-alert\"</span>");
    dialogContent.append(dialogText);
    dialog.append(dialogContent);
    $("body").append(dialog);
    $("#deleteDialog").keyup(function(e) { console.log(e); if (e.keyCode === 13) { $("#deleteDialog").next().find("button").first().trigger("click"); } })
    $("#deleteDialog").dialog({
        resizable: false,
        modal: true,
        width: 500,
        buttons: {
            "Delete song": function() { 
                var formData = new FormData();
                var xhr = new XMLHttpRequest();
                formData.append("songName", JSON.stringify($(song).data("data")));
                xhr.open("DELETE", "/library", true);
                xhr.onreadystatechange = function() {
                    if (xhr.readyState == 4 && xhr.status == 200) {
                        displayLibrary();
                    }
                }
                xhr.send(formData);
                $(this).dialog("close");
            },
            "Cancel": function() { $(this).dialog("close"); }
        },
        close: function() { $(this).remove(); }
    });
}

var renameSong = function(song) {
    var songName = $(song).data("data").title;
    var dialog = $("<div id=\"renameDialog\"></div>")
    var dialogTitle = "Rename " + songName;
    var dialogText = "Please enter the new title for \"" + songName + "\".<br><br><span id=\"newTitleErrors\"></span><br><label for=\"title\">New title: </label><input id=\"newTitle\" type=\"text\" name=\"title\" /> ";

    dialog.attr("title", dialogTitle);
    var dialogContent = $("<p></p>");
    dialogContent.append("<span class=\"ui-icon ui-icon-info\"</span>");
    dialogContent.append(dialogText);
    dialog.append(dialogContent);
    $("body").append(dialog);
    $("#newTitle").keyup(function(e) { console.log(e); if (e.keyCode === 13) { $("#renameDialog").next().find("button").first().trigger("click"); } })
    $("#renameDialog").dialog({
        resizable: false,
        modal: true,
        width: 500,
        buttons: {
            "Rename song": function() {
                if( $("#newTitle").attr("value").replace(/\s*/, "") === "" ) {
                    $("#newTitleErrors").html("Not a valid title!");
                    $("#newTitle").addClass("ui-state-error");
                } else {
                    $("#newTitleErrors").html("Valid!");
                    $("#newTitle").removeClass("ui-state-error");
                    var formData = new FormData();
                    var xhr = new XMLHttpRequest();
                    formData.append("songName", $(song).data("data").title);
                    formData.append("field", "title");
                    formData.append("content", $("#newTitle").attr("value"));
                    xhr.open("PUT", "/edit/", true);
                    xhr.onreadystatechange = function() {
                        if (xhr.readyState == 4 && xhr.status == 200) {
                            displayLibrary()
                        }
                    }
                    xhr.send(formData);
                    $(this).dialog("close");
                }
            },
            "Cancel": function() { $(this).dialog("close"); }
        },
        close: function() { $(this).remove(); }
    });
}
