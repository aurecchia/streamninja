
// playerUI is a global variable.
// append to this object everything needed globally
// Do not use this object for internal functions, create local variables instead
playerUI = new Object();

playerUI.totalTime = undefined;
playerUI.currentTime = undefined;
playerUI.setPlayIcon = function() { $( "#playpause" ).button("option", "icons", { primary: "ui-icon-play" } ); };
playerUI.setPauseIcon = function() { $( "#playpause" ).button("option", "icons", { primary: "ui-icon-pause" } ); };
playerUI.setEffectsDownIcon = function() { $( "#effectsButton").button("option", "icons", { primary: "ui-icon-arrowstop-1-s" }); };
playerUI.setEffectsUpIcon = function() { $( "#effectsButton").button("option", "icons", { primary: "ui-icon-arrowstop-1-n" }); };
playerUI.play = function(songObj) {
    var song = $(songObj)
    $(".playing").removeClass("playing");
    $(songObj).addClass("playing");
    player.play(song.data("data").path);
    playerUI.setPlayerInfo(song.data("data"));
}
playerUI.setPlayerInfo = function(song) {
    var infos = song.title + " - " + song.artist;
    $("title").html(infos);
    $("#songInfos").html(infos)
    playerUI.setPauseIcon();
}
playerUI.updateCurrentTime = function(time) {
    playerUI.currentTime = time;
    $("#slider").slider("option", "value", time);
    $("#currentTime").html( ("0" + Math.floor(time / 60)).slice(-2) + ":" + ("0" + (time % 60)).slice(-2) );
    Piecon.setProgress( (time * 100) / playerUI.totalTime );
}
playerUI.updateTotalTime = function(time) {
    $("#slider").slider("option", "max", time);
    $("#totalTime").html( ("0" + Math.floor(time / 60)).slice(-2) + ":" + ("0" + (time % 60)).slice(-2) );
    playerUI.totalTime = time;
}
playerUI.playPrev = function() {
    var temp = $(".playing");
    var prevSong = findPrevSong();
    if (temp !== undefined && prevSong !== undefined) {
        playerUI.play(findPrevSong());
        player.loadNextSong($(temp).data("data").path);
    }
}
playerUI.playNext = function() {
    var nextSong = findNextSong();
    // console.log(nextSong)
    if (nextSong !== undefined) {
        playerUI.play(nextSong);
        player.loadNextSong($(findNextSong()).data("data").path);
    }
}
playerUI.playPause = function() {
    if ($(".playing").length === 1 || playerUI.totalTime) {
        if ($("#playpause").button("option", "icons").primary === "ui-icon-play"  ) {
            playerUI.setPauseIcon();
            player.play();
        } else {
            playerUI.setPlayIcon();
            player.pause();
        }
    }
}

playerUI.shuffleLoop = function() {
    if($("#shuffleLoopButton").button("option", "icons").primary === "ui-icon-refresh") {
        $("#shuffleLoopButton").button("option", "icons", {primary: "ui-icon-shuffle"});
    } else {
        $("#shuffleLoopButton").button("option", "icons", {primary: "ui-icon-refresh"});
    }
}

playerUI.updateVisualizer = function(left, right) {
    var audioLimit = 140;
    var leftCtx = $("#left_visualizer")[0].getContext("2d");
    var rightCtx = $("#right_visualizer")[0].getContext("2d");
    leftCtx.fillStyle = "#262626";
    rightCtx.fillStyle = "#262626";
    leftCtx.fillRect(0,0,6,140);
    rightCtx.fillRect(0,0,6,140);
    leftVal = Math.round(left * 140 / audioLimit);
    rightVal = Math.round(right * 140 / audioLimit);
    leftCtx.fillStyle = "#27ABFF";
    rightCtx.fillStyle = "#27ABFF";
    leftCtx.fillRect(0, 140-leftVal ,6,leftVal);
    rightCtx.fillRect(0, 140-rightVal ,6,rightVal);
}

playerUI.updateCompReduction = function(val) { $("#red_knob_comp").val(val).trigger("change"); }

playerUI.updatePlayerUI = function(msg) {
    // ==== LOWPASS FILTER ====
    if (msg.lowpass !== undefined) {
         if (msg.lowpass.active !== undefined)
             msg.lowpass.active ? $("#effect_lpf label").first().trigger("click") : $("#effect_lpf label").last().trigger("click");
         if (msg.lowpass.frequency !== undefined) 
             $("#freq_slider_lpf").slider("option", "value", inverseChangeFrequency(msg.lowpass.frequency));
    }

    // ==== HIGHPASS FILTER ====
    if (msg.highpass !== undefined) {
        if (msg.highpass.active !== undefined)
             msg.highpass.active ? $("#effect_hpf label").first().trigger("click") : $("#effect_hpf label").last().trigger("click");
        if (msg.highpass.frequency !== undefined)
             $("#freq_slider_hpf").slider("option", "value", inverseChangeFrequency(msg.highpass.frequency));
    }

    // ==== LOWSHELF ====
    if (msg.lowshelf !== undefined) {
        if (msg.lowshelf.active !== undefined)
            msg.lowshelf.active ? $("#effect_lshelf label").first().trigger("click") : $("#effect_lshelf label").last().trigger("click");
        if (msg.lowshelf.frequency !== undefined)
             $("#freq_slider_lshelf").slider("option", "value", inverseChangeFrequency(msg.lowshelf.frequency));
        if (msg.lowshelf.gain !== undefined)
             $("#q_slider_lshelf").slider("option", "value", msg.lowshelf.gain);
    }

    // ==== HIGHSHELF ====
    if (msg.highshelf !== undefined) {
        if (msg.highshelf.active !== undefined)
            msg.highshelf.active ? $("#effect_hshelf label").first().trigger("click") : $("#effect_hshelf label").last().trigger("click");
        if (msg.highshelf.frequency !== undefined)
             $("#freq_slider_hshelf").slider("option", "value", inverseChangeFrequency(msg.highshelf.frequency));
        if (msg.highshelf.gain !== undefined)
             $("#q_slider_hshelf").slider("option", "value", msg.highshelf.gain);
    }

    // ==== PEAKING ====
    if (msg.peaking !== undefined) {
        if (msg.peaking.active !== undefined)
            msg.peaking.active ? $("#effect_peak label").first().trigger("click") : $("#effect_peak label").last().trigger("click");
        if (msg.peaking.frequency !== undefined)
             $("#freq_slider_peak").slider("option", "value", inverseChangeFrequency(msg.peaking.frequency));
        if (msg.peaking.gain !== undefined)
             $("#q_slider_peak").slider("option", "value", msg.peaking.gain);
    }

    // ==== NOTCH ====
    if (msg.notch !== undefined) {
        if (msg.notch.active !== undefined)
            msg.notch.active ? $("#effect_notch label").first().trigger("click") : $("#effect_notch label").last().trigger("click");
        if (msg.notch.frequency !== undefined)
             $("#freq_slider_notch").slider("option", "value", inverseChangeFrequency(msg.notch.frequency));
        if (msg.notch.gain !== undefined)
             $("#q_slider_notch").slider("option", "value", msg.notch.gain);
    }

    // ==== GRAPHICAL EQUALIZER ====
    if (msg.eq !== undefined) {
        if (msg.eq.active !== undefined)
            msg.eq.active ? $("#effect_eq label").first().trigger("click") : $("#effect_eq label").last().trigger("click");
        if (msg.eq.gain0)
           $("#gain0_slider_eq").slider("option", "value", msg.eq.gain0);
        if (msg.eq.gain1)
           $("#gain1_slider_eq").slider("option", "value", msg.eq.gain1);
        if (msg.eq.gain2)
           $("#gain2_slider_eq").slider("option", "value", msg.eq.gain2);
        if (msg.eq.gain3)
           $("#gain3_slider_eq").slider("option", "value", msg.eq.gain3);
        if (msg.eq.gain4)
           $("#gain4_slider_eq").slider("option", "value", msg.eq.gain4);
        if (msg.eq.gain5)
           $("#gain5_slider_eq").slider("option", "value", msg.eq.gain5);
        if (msg.eq.gain6)
           $("#gain6_slider_eq").slider("option", "value", msg.eq.gain6);
        if (msg.eq.gain7)
           $("#gain7_slider_eq").slider("option", "value", msg.eq.gain7);
        if (msg.eq.gain8)
           $("#gain8_slider_eq").slider("option", "value", msg.eq.gain8);
        if (msg.eq.gain9)
           $("#gain9_slider_eq").slider("option", "value", msg.eq.gain9);
    }

     // ==== COMPRESSOR ====
     if (msg.compressor !== undefined) {
         if (msg.compressor.active !== undefined) {
            msg.compressor.active ? $("#effect_comp label").first().trigger("click") : $("#effect_comp label").last().trigger("click");
         }
         if (msg.compressor.threshold)
           $("#thres_knob_comp").val(msg.compressor.threshold).trigger("change");
         if (msg.compressor.knee)
           $("#knee_knob_comp").val(msg.compressor.knee).trigger("change");
         if (msg.compressor.ratio)
           $("#ratio_knob_comp").val(msg.compressor.ratio).trigger("change");
         if (msg.compressor.attack)
           $("#att_knob_comp").val(msg.compressor.attack*1000).trigger("change");
         if (msg.compressor.release)
           $("#rel_knob_comp").val(msg.compressor.release*1000).trigger("change");
     }

     // ==== ANALYSER ====
     if (msg.analyser !== undefined) {
         if (msg.analyser.active !== undefined) {
            msg.analyser.active ? $("#effect_visualizer label").first().trigger("click") : $("#effect_visualizer label").last().trigger("click");
         }
     }
}


    //  taken from here (thanks html5rocks!):
    //      http://www.html5rocks.com/en/tutorials/webaudio/intro/
var FREQUENCY_MULTIPLIER = Math.log(22050 / 50) / Math.LN2;
var changeFrequency = function(value) {
    return 22050 * Math.pow(2, FREQUENCY_MULTIPLIER * (value/100 - 1));
}
var inverseChangeFrequency = function(value) {
    return 100 * (Math.log(value/22050) + FREQUENCY_MULTIPLIER*Math.LN2 ) / ( FREQUENCY_MULTIPLIER*Math.LN2); 
}


/*======================================================================================================================*/


                                                    /*DOCUMENT ONLOAD*/


/*======================================================================================================================*/


window.addEventListener('load', function(e) {

    $("#effectsPane").tooltip();

	// create buttons
	$( "#playpause" ).button( {icons: {primary: "ui-icon-play" }, text: false} );
    $( "#next" ).button( {icons: {primary: "ui-icon-seek-next" }, text: false} );
    $( "#prev" ).button( {icons: {primary: "ui-icon-seek-prev" }, text: false} );

    $( "#effectsButton" ).button( {icons: {primary: "ui-icon-arrowstop-1-s"}, text: true} );
    $( "#shuffleLoopButton" ).button( {icons: {primary: "ui-icon-refresh"}, text: false} );
    $("#shuffleLoopButton").bind("click", function() { playerUI.shuffleLoop() });

    Piecon.setOptions({ color: "#27ABFF", background: "#262626", fallback: 'false'});

    // ================== Seeking ==================
    $("#slider").slider( {range: "min",
        value: 0,
        min: 0,
        max: 100,	// note that the max is set when each song is loaded, (see player.js, on message, on timeupdate)
        animate: "fast",
        slide: function (event, ui) {
            player.seeking(ui.value);
            playerUI.setPauseIcon();
        }
    });

    // ================== Volume ==================
    $("#slider_volume").slider( {
        range: "min",
        value: 0,
        min: -40,
        max: 0,
        animate: "fast",
        slide: function(event, ui) {
            player.send({volume:ui.value});
        }
    });

    // ================== Media Buttons ==================
    // ==== Play/Pause ====
    $("#playpause").bind("click", playerUI.playPause);

    // bind for next song
    $("#next").bind("click", playerUI.playNext);

    // bind previous song
    $("#prev").bind("click", playerUI.playPrev);

    // ================== Low Pass Filter ==================

    $(".freqSlider").slider( {
        range: "min",
        value: 50,
        min: 0,
        max: 100,
        animate: "fast",
        orientation: "vertical"
    } );

    $(".resSlider").slider( {
        range: "min",
        value: 50,
        min: 0,
        max: 100,
        animate: "fast",
        orientation: "vertical"
    } );
    
    $(".gainSlider").slider( {
        range: "min",
        value: 0,
        min: -40,
        max: 40,
        animate: "fast",
        orientation: "vertical"
    } );

    // var tooltip = $("<div class=tooltip />");

    // Attach tooltups to sliders
    $(".freqSlider").find(".ui-slider-handle").after($("<div class='tooltip'/>").html(Math.floor(changeFrequency(50))+" Hz").hide());
    $(".gainSlider").find(".ui-slider-handle").after($("<div class='tooltip'/>").html("+0 dB").hide());

    // Set tooltips to show on hover and move with handle
    $(".effectSlider").on( "slidestart", function() { 
        var position = $(this).find(".ui-slider-handle").position();
        $(this).find(".ui-slider-handle").next().css("top", position.top - 3).css("left", position.left + 28).show();
    });
    $(".effectSlider").on( "slidestop", function() { $(this).find(".ui-slider-handle").next().hide(); });
    $(".effectSlider").on( "slide", function() {
        var position = $(this).find(".ui-slider-handle").position();
        $(this).find(".ui-slider-handle").next().css("top", position.top - 3).css("left", position.left + 28);
    });


    // LOW PASS FUNCTIONS
    $("#effect_lpf .toggle select").toggleSwitch({ change: function(event, ui) { player.send({lowpass:{active: ui.value !== 100 }}) } });
    $("#freq_slider_lpf").slider("option", "slide", function(event, ui) { var freq = changeFrequency(ui.value); player.send({lowpass:{frequency:freq}}); $("#freq_slider_lpf .tooltip").html(Math.floor(freq)+" Hz"); } );

    // HIGH PASS FUNCTIONS
    $("#effect_hpf .toggle select").toggleSwitch({ change: function(event, ui) { player.send({highpass:{active: ui.value !== 100 }}) } });
    $("#freq_slider_hpf").slider("option", "slide", function(event, ui) { var freq = changeFrequency(ui.value); player.send({highpass:{frequency:freq}}); $("#freq_slider_hpf .tooltip").html(Math.floor(freq)+" Hz"); } );
    
    // LOW SHELF FUNCTIONS
    $("#effect_lshelf .toggle select").toggleSwitch({ change: function(event, ui) { player.send({lowshelf:{active: ui.value !== 100 }}) } });
    $("#freq_slider_lshelf").slider("option", "slide", function(event, ui) { var freq = changeFrequency(ui.value); player.send({lowshelf:{frequency:freq}}); $("#freq_slider_lshelf .tooltip").html(Math.floor(freq)+" Hz"); });
    $("#q_slider_lshelf").slider("option", "slide", function(event, ui) { $("#q_slider_lshelf .tooltip").html((ui.value>=0?"+":"")+ui.value+" dB"); player.send({lowshelf:{gain:ui.value}}); });
    
    // HIGH SHELF FUNCTIONS
    $("#effect_hshelf .toggle select").toggleSwitch({ change: function(event, ui) { player.send({highshelf:{active: ui.value !== 100 }}) } });
    $("#freq_slider_hshelf").slider("option", "slide", function(event, ui) { var freq = changeFrequency(ui.value); player.send({highshelf:{frequency:freq}}); $("#freq_slider_hshelf .tooltip").html(Math.floor(freq)+" Hz"); });
    $("#q_slider_hshelf").slider("option", "slide", function(event, ui) { $("#q_slider_hshelf .tooltip").html((ui.value>=0?"+":"")+ui.value+" dB"); player.send({highshelf:{gain:ui.value}}); });

    // PEAKING FUNCTIONS
    $("#effect_peak .toggle select").toggleSwitch({ change: function(event, ui) { player.send({peaking:{active: ui.value !== 100 }}) } });
    $("#freq_slider_peak").slider("option", "slide", function(event, ui) { var freq = changeFrequency(ui.value); player.send({peaking:{frequency:freq}}); $("#freq_slider_peak .tooltip").html(Math.floor(freq)+" Hz"); });
    $("#q_slider_peak").slider("option", "slide", function(event, ui) { $("#q_slider_peak .tooltip").html((ui.value>=0?"+":"")+ui.value+" dB"); player.send({peaking:{gain:ui.value}}); });
    
    // NOTCH FUNCTIONS
    $("#effect_notch .toggle select").toggleSwitch({ change: function(event, ui) { player.send({notch:{active: ui.value !== 100 }}) } });
    $("#freq_slider_notch").slider("option", "slide", function(event, ui) { var freq = changeFrequency(ui.value); player.send({notch:{frequency:freq}}); $("#freq_slider_notch .tooltip").html(Math.floor(freq)+" Hz"); });
    $("#q_slider_notch").slider("option", "slide", function(event, ui) { $("#q_slider_notch .tooltip").html((ui.value>=0?"+":"")+ui.value+" dB"); player.send({notch:{gain:ui.value}}); });
    
    // EQ FUNCTIONS
    $("#effect_eq .toggle select").toggleSwitch({ change: function(event, ui) {  player.send({eq:{active: ui.value !== 100 }}) } });

    $("#gain0_slider_eq").slider("option", "slide", function(event, ui) { $("#gain0_slider_eq .tooltip").html((ui.value>=0?"+":"")+ui.value+" dB"); player.send({eq:{gain0:ui.value}}); });
    $("#gain1_slider_eq").slider("option", "slide", function(event, ui) { $("#gain1_slider_eq .tooltip").html((ui.value>=0?"+":"")+ui.value+" dB"); player.send({eq:{gain1:ui.value}}); });
    $("#gain2_slider_eq").slider("option", "slide", function(event, ui) { $("#gain2_slider_eq .tooltip").html((ui.value>=0?"+":"")+ui.value+" dB"); player.send({eq:{gain2:ui.value}}); });
    $("#gain3_slider_eq").slider("option", "slide", function(event, ui) { $("#gain3_slider_eq .tooltip").html((ui.value>=0?"+":"")+ui.value+" dB"); player.send({eq:{gain3:ui.value}}); });
    $("#gain4_slider_eq").slider("option", "slide", function(event, ui) { $("#gain4_slider_eq .tooltip").html((ui.value>=0?"+":"")+ui.value+" dB"); player.send({eq:{gain4:ui.value}}); });
    $("#gain5_slider_eq").slider("option", "slide", function(event, ui) { $("#gain5_slider_eq .tooltip").html((ui.value>=0?"+":"")+ui.value+" dB"); player.send({eq:{gain5:ui.value}}); });
    $("#gain6_slider_eq").slider("option", "slide", function(event, ui) { $("#gain6_slider_eq .tooltip").html((ui.value>=0?"+":"")+ui.value+" dB"); player.send({eq:{gain6:ui.value}}); });
    $("#gain7_slider_eq").slider("option", "slide", function(event, ui) { $("#gain7_slider_eq .tooltip").html((ui.value>=0?"+":"")+ui.value+" dB"); player.send({eq:{gain7:ui.value}}); });
    $("#gain8_slider_eq").slider("option", "slide", function(event, ui) { $("#gain8_slider_eq .tooltip").html((ui.value>=0?"+":"")+ui.value+" dB"); player.send({eq:{gain8:ui.value}}); });
    $("#gain9_slider_eq").slider("option", "slide", function(event, ui) { $("#gain9_slider_eq .tooltip").html((ui.value>=0?"+":"")+ui.value+" dB"); player.send({eq:{gain9:ui.value}}); });
    

    // COMPRESSOR FUNCTIONS
    $("#effect_comp .toggle select").toggleSwitch({ change: function(event, ui) {
            player.send({compressor:{active: ui.value!==100 }})
            $("#red_knob_comp").val(-20).trigger("change");
    }});
    $("#thres_knob_comp").knob({ "min": -100, "max": 0, width: 60, height: 60, fgColor: "#27ABFF", bgColor: "#262626" })
    $("#knee_knob_comp").knob({ "min": 0, "max": 40, width: 60, height: 60, fgColor: "#27ABFF", bgColor: "#262626"  })
    $("#ratio_knob_comp").knob({ "min": 1, "max": 20, width: 60, height: 60, fgColor: "#27ABFF", bgColor: "#262626"  })
    $("#red_knob_comp").knob({ "min": -20, "max": 0, width: 60, height: 60, readOnly: true, fgColor: "#27ABFF", bgColor: "#262626"  })
    $("#att_knob_comp").knob({ "min": 0, "max": 1000, width: 60, height: 60, fgColor: "#27ABFF", bgColor: "#262626"  })
    $("#rel_knob_comp").knob({ "min": 0, "max": 1000, width: 60, height: 60, fgColor: "#27ABFF", bgColor: "#262626"  })

    $("#thres_knob_comp").trigger("configure", { change: function(val) {
        val = (val === -101 ? -100 : val);
        val = (val === 1 ? 0 : val);
        player.send({compressor:{threshold:val}});
    } });
    $("#knee_knob_comp").trigger("configure", { change: function(val) { 
        val = (val === -1 ? 0 : val);
        val = (val === 41 ? 40 : val);
        player.send({compressor:{knee:val}});
    } });
    $("#ratio_knob_comp").trigger("configure", { change: function(val) { 
        val = (val === 0 ? 1 : val);
        val = (val === 21 ? 20 : val);
        player.send({compressor:{ratio:val}});
    } });
    $("#red_knob_comp").trigger("configure", { "angleOffset": 225, "angleArc": 270 });
    $("#att_knob_comp").trigger("configure", { change: function(val) { 
        val = (val === -1 ? 0 : val);
        val = (val === 1001 ? 1000 : val);
        player.send({compressor:{attack:val/1000}});
    } });
    $("#rel_knob_comp").trigger("configure", { change: function(val) { 
        val = (val === -1 ? 0 : val);
        val = (val === 1001 ? 1000 : val);
        player.send({compressor:{release:val/1000}});
    } });


    $("#effect_visualizer .toggle select").toggleSwitch({ change: function(event, ui) {  player.send({analyser:{active: ui.value!==100 }}) }});

    window.addEventListener('keydown', function(event) {
        switch (event.keyCode) {
            case 32: // spacebar
                playerUI.playPause();
                if (e.stopPropagation) {
                    e.stopPropagation();
                    e.preventDefault();
                }
                break;
            case 37: // Left
                playerUI.playPrev();
                break
            case 39: // Right
                playerUI.playNext()
                break;
        }
    }, false);

}, false);
