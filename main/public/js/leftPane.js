// leftPane is a global variable.
// append to this object everything needed globally
// Do not use this object for internal functions, create local variables instead
leftPane = new Object();

$(function(){
        $("#playlistBox").bind("click", function() { $(this).next("ul").slideToggle(300); });
        $("#usersBox").bind("click", function() { $(this).next("ul").slideToggle(300); });
        $("#friendsPlaylistBox").bind("click", function() { $(this).next("ul").slideToggle(300); });
})



leftPane.removePLaylist = function(obj) {
    var xhr = new XMLHttpRequest();
    xhr.open("DELETE", "/deleteplaylist/" + $(obj).data("data").name + "/" + getId(), true);
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            $(obj).remove();
        }
    }
    xhr.send(null);
}


window.addEventListener('load', function(e) {

    leftPane.removePLaylist = function(obj) {
        var xhr = new XMLHttpRequest();
        xhr.open("DELETE", "/deleteplaylist/" + $(obj).data("data").name + "/" + getId(), true);
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4 && xhr.status == 200) {
                $(obj).remove();
            }
        }
        xhr.send(null);
    }
    
    //send the message to the chatBox
    $("#chatMessage").bind("keypress", function(e) {
        //indetinfy the key pressed
        var keycode = (event.keyCode ? event.keyCode : event.which);
        //check if enter was pressed
        if(keycode==13) {
            //send the message to the socket
            socketClient.send('chatMessage', {"id": getId(), "name" : getName(), "message" :$("#chatMessage").val()});
            //reset the textarea
            $("#chatMessage").val("");
            //prevent to go to a new line
            event.preventDefault();
        }
    });

    //create a new room
    leftPane.newRoom = function() {
        var dialog = $("<div id=\"newRoomDialog\"></div>")
        var dialogTitle = "New Room";
        var dialogText = "Please enter the name for the new room.<br><br><span id=\"roomNameErrors\"></span><br><label for=\"roomName\">Room name </label><input id=\"roomName\" type=\"text\" name=\"roomName\" /> ";

        dialog.attr("title", dialogTitle);
        var dialogContent = $("<p></p>");
        dialogContent.append("<span class=\"ui-icon ui-icon-info\"</span>");
        dialogContent.append(dialogText);
        dialog.append(dialogContent);
        $("body").append(dialog);
        $("#roomName").keyup(function(e) { console.log(e); if (e.keyCode === 13) { $("#newRoomDialog").next().find("button").first().trigger("click"); } })
        $("#newRoomDialog").dialog({
            resizable: false,
            modal: true,
            width: 500,
            buttons: {
                "Create room": function() {
                    if( $("#roomName").attr("value").replace(/\s*/, "") === "" ) {
                        $("#roomNameErrors").html("Not a room name!");
                        $("#roomName").addClass("ui-state-error");
                    } else {
                        $("#roomNameErrors").html("Valid!");
                        $("#roomName").removeClass("ui-state-error");
                        $("#chatMessagesList").children("li").remove();
            			var playerStatus = player.getStatus();
                        playerStatus.currentTime = playerUI.currentTime;
            			playerStatus.data = $(".playing").data("data");
                        socketClient.send('newRoom', {"name":getName(), "id":getId(), "room":$("#roomName").attr("value"), "status":playerStatus});
                        $(this).dialog("close");
                    }
                },
                "Cancel": function() { $(this).dialog("close"); }
            },
            close: function() { $(this).remove(); }
        });
    }
    
    leftPane.joinRoom = function(room) {
        socketClient.send('changeRoom', {"name":getName(), "id": getId(), "room":room});
        $("#chatMessagesList").children("li").remove();
    }

    leftPane.displayUserPlaylist = function() {
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "/userPlaylist/"+getId(), true);
        xhr.onreadystatechange = function() {
            if(xhr.readyState == 4) {
                if(xhr.status == 200) {
                    var playlists=JSON.parse(xhr.responseText);
                    var list = $("#playlistList");
                    list.children("li").remove();
		    $("#friendsPlaylists").children("li").remove();
                    for(i = 0; i < playlists.length; i++) {
                        var name = playlists[i].name;
                        var owner= playlists[i].owner;
                        var removeButton = $("<div class=\"playlistRemoveButton\">X</div>");
                        removeButton.bind("click", function() { leftPane.removePLaylist($(this).parent()) });
                        var item = $("<li class=\"playlist\" onclick=\"leftPane.getPlaylist('"+name+"','"+owner+"')\" >"+name+"</li>");
                        item.prepend(removeButton);
                        item.data("data", playlists[i]);
                        list.append(item);
                    }
                    leftPane.getFriendPlaylists(getFriends());
                }
            }
        }
        xhr.send(null);
    }
       

    leftPane.getFriendPlaylists = function(friends) {
        var xhr = new XMLHttpRequest();
        var formData = new FormData();
        formData.append("friends", friends);
        friends = JSON.parse(friends);
        xhr.open("POST", "/userPlaylist/", true);
        xhr.onreadystatechange = function() {
            if(xhr.readyState ==4) {
                if(xhr.status == 200) {
                    var playlists = JSON.parse(xhr.responseText);
                    $("#friendsPlaylists").html("");
                    for(var friend in playlists){
                        var friendItem = $("<li></li>");
                        var friendObject = getFriendById(playlists[friend][0].owner);
                        var friendName = $("<div class=\"friendName\">"+getFriendById(playlists[friend][0].owner).name+"</div>");
                        friendName.bind("click", function() { $(this).next().slideToggle(300); })
                        var playlistsUl = $("<ul></ul>");
                        for(var playlist in playlists[friend]){
                            var name= playlists[friend][playlist].name;
                            var owner= playlists[friend][playlist].owner;
                            var item = $("<li class=\"playlist\" onclick=\"leftPane.getPlaylist('"+name+"','"+owner+"')\" >"+name+"</li>");
                            item.data("data", friends);
                            playlistsUl.append(item);
                        }     
                        friendItem.append(friendName);
                        friendItem.append(playlistsUl);
                        $("#friendsPlaylists").append(friendItem);
                    }
                } 

            }
        }
        xhr.send(formData);
    }

    
    leftPane.getPlaylist = function(name, owner) {
        xhr = new XMLHttpRequest();
        xhr.open("GET", "/playlist/"+name+"/"+owner, true);
        xhr.onreadystatechange = function() {
            if(xhr.readyState == 4) {
                if(xhr.status == 200) {
                    playlists = JSON.parse(xhr.responseText)
                    updatePlaylist(JSON.parse(xhr.responseText)[0]);
                    $("#activePlaylistTitle").text(name);
                }
                else if(name ==="default" && xhr.status == 404) {
                    rightPane.newPlaylist("default");
                    rightPane.actualPlaylist.name=name;
                    rightPane.actualPlaylist.owner =getId();
                }
            }
        }
        xhr.send(null);
    }
    
	var updatePlaylist = function(playlist) {	    
        $("#activePlaylist").children("li").remove();
        rightPane.actualPlaylist.name = playlist.name;
        rightPane.actualPlaylist.owner = playlist.owner;
        var data = playlist.songs;
	    for(i= 0; i < playlist.songs.length; i++) {
            
            var button = $("<div id=\"listButton_" + elementNumber +"\" class=\"playlistRemoveButton\">X</div>");
            button.bind("click", function() {rightPane.removeSong([$(this).parent().data("data").title]); $(this).parent().remove() });
            
            var title = $("<p>" + data[i].title + "</p>");
            
            var song = $("<li id=\"listItem_" + elementNumber + "\" draggable=\"true\"></li>");

            song.data("data", data[i]);
            song.bind("dblclick", function() { playerUI.play(this) });
            song.append(button);
            song.append(title);
            
            $("#activePlaylist").append(song);
        }
	}
});
		       
leftPane.attachUsersMenu = function(userElement) {
    console.log( userElement.attr("id"))
    $.contextMenu({
        selector: "#" + userElement.attr("id"),
        items: {
            "Promote to admin": {name: "Promote to admin", callback: function(key, opt) { promoteToAdmin(opt.$trigger) } },
            "Kick from room": {name: "Kick from room", callback: function(key, opt) { leftPane.kickUser(opt.$trigger) } }
        }
    });

}

var promoteToAdmin = function(userBox) {
    var dialog = $("<div id=\"promoteDialog\"></div>")
    var dialogTitle = "Promote " + $(userBox).html();
    var dialogText = "Are you sure you want to give\"" + ($(userBox)).html() + "\" administration privileges on this room?.<br><br>This decision cannot be reverted.";

    dialog.attr("title", dialogTitle);
    var dialogContent = $("<p></p>");
    dialogContent.append("<span class=\"ui-icon ui-icon-alert\"</span>");
    dialogContent.append(dialogText);
    dialog.append(dialogContent);
    $("body").append(dialog);
    $("#promoteDialog").keyup(function(e) { console.log(e); if (e.keyCode === 13) { $("#renameDialog").next().find("button").first().trigger("click"); } })
    $("#promoteDialog").dialog({
        resizable: false,
        modal: true,
        width: 500,
        buttons: {
            "Give privileges": function() {
                $(userBox).addClass("admin");
                socketClient.send("addAdmin", {"id": $(userBox).attr("id"), "room": $(userBox).parent().parent().attr("id").substr(5)});
                $(this).dialog("close");
            },
            "Cancel": function() { $(this).dialog("close"); }
        },
        close: function() { $(this).remove(); }
    });
}

