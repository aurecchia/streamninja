/*
How nodes and effects are handled:
	Each Web Audio API node is encapsulated in a Node object.
	nodesArray is an array of Node objects, where the first element is the source, and the last is the destination.
	n is a Object used as dictionary to retrieve easily the indexes of specific effects

About parent-frame communication:
	Communication has to be done with JSON objects.
	about the "active" thing: true or false

	examples:
	activate lowpass and set frequency:
		"{lowpass:{active:true, frequency:440}}"

	play the song:
		"{play:true}"

	pause the song:
		"{play:false}"

	play and set current time:
		"{play:true, currentTime:123}"

	change volume:
		"{volume:0}"
*/

var context;
var audio;

var nodesArray = new Array();
var n = new Object();

var analyser = undefined;

window.addEventListener('message', function postMessageListener(e) {
	if (audio === undefined) {
		setTimeout(function() { postMessageListener(e) }, 50);
		return;
	}
	var msg = JSON.parse(e.data);
	// ==== PLAY ====
	if (msg.play !== undefined) {
		if (msg.play) {
			audio.play();
			sendToParent({songLength:audio.duration});
		} else {
			audio.pause();
		}
	}

	// ==== VOLUME ====
	if (msg.volume !== undefined) { 
		nodesArray[n["gain"]].mediaElement.gain.value = msg.volume;
	}

	// ==== CURRENT TIME ====
	if (msg.currentTime !== undefined) { audio.currentTime = msg.currentTime; }

	// ==== LOWPASS FILTER ====
	if (msg.lowpass !== undefined) {
		if (msg.lowpass.active !== undefined)
			msg.lowpass.active ? enable("lowpass") : disable("lowpass");
		if (msg.lowpass.frequency !== undefined) 
			nodesArray[n["lowpass"]].mediaElement.frequency.value = msg.lowpass.frequency;
	}

	// ==== HIGHPASS FILTER ====
	if (msg.highpass !== undefined) {
		if (msg.highpass.active !== undefined)
			msg.highpass.active ? enable("highpass") : disable("highpass");
		if (msg.highpass.frequency !== undefined)
			nodesArray[n["highpass"]].mediaElement.frequency.value = msg.highpass.frequency;
	}

	// ==== LOWSHELF ====
	if (msg.lowshelf !== undefined) {
		if (msg.lowshelf.active !== undefined)
			msg.lowshelf.active ? enable("lowshelf") : disable("lowshelf");
		if (msg.lowshelf.frequency !== undefined)
			nodesArray[n["lowshelf"]].mediaElement.frequency.value = msg.lowshelf.frequency;
		if (msg.lowshelf.gain !== undefined)
			nodesArray[n["lowshelf"]].mediaElement.gain.value = msg.lowshelf.gain;
	}

	// ==== HIGHSHELF ====
	if (msg.highshelf !== undefined) {
		if (msg.highshelf.active !== undefined)
			msg.highshelf.active ? enable("highshelf") : disable("highshelf");
		if (msg.highshelf.frequency !== undefined)
			nodesArray[n["highshelf"]].mediaElement.frequency.value = msg.highshelf.frequency;
		if (msg.highshelf.gain !== undefined)
			nodesArray[n["highshelf"]].mediaElement.gain.value = msg.highshelf.gain;
	}

	// ==== PEAKING ====
	if (msg.peaking !== undefined) {
		if (msg.peaking.active !== undefined)
			msg.peaking.active ? enable("peaking") : disable("peaking");
		if (msg.peaking.frequency !== undefined)
			nodesArray[n["peaking"]].mediaElement.frequency.value = msg.peaking.frequency;
		if (msg.peaking.gain !== undefined)
			nodesArray[n["peaking"]].mediaElement.gain.value = msg.peaking.gain;
	}

	// ==== NOTCH ====
	if (msg.notch !== undefined) {
		if (msg.notch.active !== undefined)
			msg.notch.active ? enable("notch") : disable("notch");
		if (msg.notch.frequency !== undefined)
			nodesArray[n["notch"]].mediaElement.frequency.value = msg.notch.frequency;
		if (msg.notch.gain !== undefined)
			nodesArray[n["notch"]].mediaElement.gain.value = msg.notch.gain;
	}

	// ==== GRAPHICAL EQUALIZER ====
	if (msg.eq !== undefined) {
		if (msg.eq.active !== undefined)
			msg.eq.active ? enable("eq") : disable("eq");
		if (msg.eq.gain0)
			nodesArray[n["eq"]].mediaElement[0].gain.value = msg.eq.gain0;
		if (msg.eq.gain1)
			nodesArray[n["eq"]].mediaElement[1].gain.value = msg.eq.gain1;
		if (msg.eq.gain2)
			nodesArray[n["eq"]].mediaElement[2].gain.value = msg.eq.gain2;
		if (msg.eq.gain3)
			nodesArray[n["eq"]].mediaElement[3].gain.value = msg.eq.gain3;
		if (msg.eq.gain4)
			nodesArray[n["eq"]].mediaElement[4].gain.value = msg.eq.gain4;
		if (msg.eq.gain5)
			nodesArray[n["eq"]].mediaElement[5].gain.value = msg.eq.gain5;
		if (msg.eq.gain6)
			nodesArray[n["eq"]].mediaElement[6].gain.value = msg.eq.gain6;
		if (msg.eq.gain7)
			nodesArray[n["eq"]].mediaElement[7].gain.value = msg.eq.gain7;
		if (msg.eq.gain8)
			nodesArray[n["eq"]].mediaElement[8].gain.value = msg.eq.gain8;
		if (msg.eq.gain9)
			nodesArray[n["eq"]].mediaElement[9].gain.value = msg.eq.gain9;
	}

	// ==== COMPRESSOR ====
	if (msg.compressor !== undefined) {
		if (msg.compressor.active !== undefined) {
			if (msg.compressor.active) {
				enable("compressor");
				clearInterval(nodesArray[n["compressor"]].interval);
				nodesArray[n["compressor"]].interval = setInterval(nodesArray[n["compressor"]].sendReduction, 32);
			} else {
				disable("compressor");
				clearInterval(nodesArray[n["compressor"]].interval);
			}
		}
			msg.compressor.active ? enable("compressor") : disable("compressor");
		if (msg.compressor.threshold)
			nodesArray[n["compressor"]].mediaElement.threshold.value = msg.compressor.threshold;
		if (msg.compressor.knee)
			nodesArray[n["compressor"]].mediaElement.knee.value = msg.compressor.knee;
		if (msg.compressor.ratio)
			nodesArray[n["compressor"]].mediaElement.ratio.value = msg.compressor.ratio;
		if (msg.compressor.attack)
			nodesArray[n["compressor"]].mediaElement.attack.value = msg.compressor.attack;
		if (msg.compressor.release)
			nodesArray[n["compressor"]].mediaElement.release.value = msg.compressor.release;
	}

	// ==== ANALYSER ====
	if (msg.analyser !== undefined) {
		if (msg.analyser.active !== undefined) {
			if (msg.analyser.active) {
				clearInterval(analyser.interval);
				analyser.interval = setInterval(analyser.sendAnalysis, 32);
			} else {
				clearInterval(analyser.interval);
			}
		}
	}

}, false);

var sendToParent = function(msg) {
	parent.postMessage(JSON.stringify(msg), "http://streamninja.inf.usi.ch:3000");
}

window.addEventListener('load', function(e) {
	context = new webkitAudioContext();
	audio = document.getElementsByTagName("audio")[0];

	// for all the filters, 3 parameters:
	// frequency. default = 350HZ, range from 10 to the Nyquist frequency (half the sample rate)
	// Q. default = 1. range from 0.0001 to 1000
	// gain. default = 0. with range from -40 to 40

	var filters = context.createBiquadFilter();

	// ==== SOURCE NODE ====
	var source = new Node( context.createMediaElementSource(audio) );
	source.active = true;
	n["source"] = nodesArray.push( source ) - 1;

	// ==== LOWPASS FILTER ====
	var lowpass = new Node( context.createBiquadFilter() );
	lowpass.mediaElement.type = filters.LOWPASS;
	n["lowpass"] = nodesArray.push( lowpass ) - 1;

	// ==== HIGHPASS FILTER ====
	var highpass = new Node( context.createBiquadFilter() );
	highpass.mediaElement.type = filters.HIGHPASS;
	n["highpass"] = nodesArray.push( highpass ) - 1;

	// ==== LOWSHELF FILTER ====
	var lowshelf = new Node( context.createBiquadFilter() );
	lowshelf.mediaElement.type = filters.LOWSHELF;
	n["lowshelf"] = nodesArray.push( lowshelf ) - 1;

	// ==== HIGHSHELF FILTER ====
	var highshelf = new Node( context.createBiquadFilter() );
	highshelf.mediaElement.type = filters.HIGHSHELF;
	n["highshelf"] = nodesArray.push( highshelf ) - 1;

	// ==== PEAKING FILTER ====
	var peaking = new Node( context.createBiquadFilter() );
	peaking.mediaElement.type = filters.PEAKING;
	n["peaking"] = nodesArray.push( peaking ) - 1;

	// ==== NOTCH FILTER ====
	var notch = new Node( context.createBiquadFilter() );
	notch.mediaElement.type = filters.NOTCH;
	n["notch"] = nodesArray.push( notch ) - 1;

	// ==== EQUALIZER ====
	var eq = new Node( undefined );
	eq.mediaElement = new Array();
	// 32 HZ, lowshelf
	eq.mediaElement.push( context.createBiquadFilter() );
	eq.mediaElement[0].type = filters.LOWSHELF;
	eq.mediaElement[0].frequency.value = 32;
	// 64 HZ, peaking
	eq.mediaElement.push( context.createBiquadFilter() );
	eq.mediaElement[1].type = filters.PEAKING;
	eq.mediaElement[1].frequency.value = 64;
	// 125 HZ, peaking
	eq.mediaElement.push( context.createBiquadFilter() );
	eq.mediaElement[2].type = filters.PEAKING;
	eq.mediaElement[2].frequency.value = 125;
	// 250 HZ, peaking
	eq.mediaElement.push( context.createBiquadFilter() );
	eq.mediaElement[3].type = filters.PEAKING;
	eq.mediaElement[3].frequency.value = 250;
	// 500 HZ, peaking
	eq.mediaElement.push( context.createBiquadFilter() );
	eq.mediaElement[4].type = filters.PEAKING;
	eq.mediaElement[4].frequency.value = 500;
	// 1K HZ, peaking
	eq.mediaElement.push( context.createBiquadFilter() );
	eq.mediaElement[5].type = filters.PEAKING;
	eq.mediaElement[5].frequency.value = 1000;
	// 2K HZ, peaking
	eq.mediaElement.push( context.createBiquadFilter() );
	eq.mediaElement[6].type = filters.PEAKING;
	eq.mediaElement[6].frequency.value = 2000;
	// 4K HZ, peaking
	eq.mediaElement.push( context.createBiquadFilter() );
	eq.mediaElement[7].type = filters.PEAKING;
	eq.mediaElement[7].frequency.value = 4000;
	// 8K HZ, peaking
	eq.mediaElement.push( context.createBiquadFilter() );
	eq.mediaElement[8].type = filters.PEAKING;
	eq.mediaElement[8].frequency.value = 8000;
	// 16K HZ, highshelf
	eq.mediaElement.push( context.createBiquadFilter() );
	eq.mediaElement[9].type = filters.HIGHSHELF;
	eq.mediaElement[9].frequency.value = 16000;
	// wire all nodes
	eq.mediaElement[0].connect(eq.mediaElement[1]);
	eq.mediaElement[1].connect(eq.mediaElement[2]);
	eq.mediaElement[2].connect(eq.mediaElement[3]);
	eq.mediaElement[3].connect(eq.mediaElement[4]);
	eq.mediaElement[4].connect(eq.mediaElement[5]);
	eq.mediaElement[5].connect(eq.mediaElement[6]);
	eq.mediaElement[6].connect(eq.mediaElement[7]);
	eq.mediaElement[7].connect(eq.mediaElement[8]);
	eq.mediaElement[8].connect(eq.mediaElement[9]);
	// change the methods
	eq.connect = function(node) { eq.mediaElement[9].connect(node.getInput()); };
	eq.getInput = function() { return eq.mediaElement[0]; };
	eq.disconnect = function() { eq.mediaElement[9].disconnect(); };

	n["eq"] = nodesArray.push( eq ) - 1;


	// ==== COMPRESSOR ====
	var compressor = new Node( context.createDynamicsCompressor() );
	compressor.interval = undefined;
	compressor.sendReduction = function() {
		sendToParent({compressor:{reduction:compressor.mediaElement.reduction.value}});
	}
	n["compressor"] = nodesArray.push ( compressor ) - 1;

	// ==== GAIN NODE ====
	var gain = new Node( context.createBiquadFilter() );
	gain.mediaElement.type = filters.HIGHSHELF;
	gain.mediaElement.frequency.value = 10;
	// gain.mediaElement.gain.value = 0;		// gain value is in range from -40 to 0 (and up to +40)
	gain.active = true;
	n["gain"] = nodesArray.push( gain ) - 1;

	// ==== DESTINATION NODE ====
	var destination = new Node( context.destination );
	destination.active = true;
	n["destination"] = nodesArray.push( destination ) - 1;

	// ==== analyser ====
	analyser = new Node( undefined );
	analyser.splitter = context.createChannelSplitter();
	analyser.left = context.createAnalyser();
	analyser.left.smoothingTimeConstant = 0.3;
	analyser.left.fftSize = 1024;
	analyser.right = context.createAnalyser();
	analyser.right.smoothingTimeConstant = 0.3;
	analyser.right.fftSize = 1024;
	analyser.splitter.connect(analyser.left, 0, 0);
	analyser.splitter.connect(analyser.right, 1, 0);
	analyser.connect = function(node) { console.error("should never need analyser output")};
	analyser.getInput = function() { return analyser.splitter; };
	analyser.disconnect = function() { console.error("should never disconnect analyser")};
	analyser.interval = undefined;
	analyser.sendAnalysis = function() {
		var arrayRight = new Uint8Array(analyser.right.frequencyBinCount);
		var arrayLeft = new Uint8Array(analyser.left.frequencyBinCount);
		analyser.right.getByteFrequencyData(arrayRight);
		analyser.left.getByteFrequencyData(arrayLeft);
		sendToParent({analyser:{left:average(arrayLeft), right:average(arrayRight)}});
	}
	
	// ==== DO THE WIRING ====
	// source --> gain node
	nodesArray[n["source"]].connect(nodesArray[n["gain"]]);
	nodesArray[n["source"]].nextActiveNode = nodesArray[n["gain"]];

	// gain node --> destination
	nodesArray[n["gain"]].connect(nodesArray[n["destination"]]);
	nodesArray[n["gain"]].nextActiveNode = nodesArray[n["destination"]];

	// gain node -> visualizer
	nodesArray[n["gain"]].connect(analyser);

	// notify the parent that we are done loading
	setTimeout(function() { sendToParent({loaded:true}) }, 50);

	audio.addEventListener("ended", function(e) {
		sendToParent({ended:true})
	}, false);

	audio.addEventListener("timeupdate", function(e) {
		sendToParent({timeupdate:audio.currentTime});
	}, false);

}, false);

var average = function(array) {
	var ret = 0;
	for (var i = 0; i < array.length; i++) {
		ret += array[i];
	}
	return ret / array.length;
}

var disable = function(nodeName) {
	if (nodeName==undefined) { console.error("disable(undefined) -> ERROR"); return; }
	
	// get myself
	var nodeIndex = n[nodeName];
	var node = nodesArray[nodeIndex];

	// don't do anything if we were already disabled
	if (node.active==false) { return; }

	// find the previous active node
	var previousNodeIndex = nodeIndex-1;
	while ( !nodesArray[previousNodeIndex].active ) {
		previousNodeIndex--;	// this loop will eventually get to an end, since nodesArray[0].active must be true
	}
	var previousNode = nodesArray[previousNodeIndex];

	// get the next node in the chain
	var nextNode = node.nextActiveNode;

	// do the wiring
	node.disconnect();								// me -> null
	previousNode.disconnect();						// prev -> null
	previousNode.connect(nextNode);					// prev -> next
	
	// update fields
	previousNode.nextActiveNode = nextNode;			// prev -> next
	node.nextActiveNode = undefined;				// me -> null

	node.active = false;

	// printNodes();
}

var printNodes = function() {
	// print all the active nodes
	var ret = "";
	var node = nodesArray[n["source"]];
	while (node !== undefined) {
		ret += node.mediaElement;
		ret += " -> ";
		node = node.nextActiveNode;
	}
	console.log(ret);
}

var enable = function(nodeName) {
	if (nodeName==undefined) { console.error("enable(undefined) -> ERROR"); return; }

	// get myself
	var nodeIndex = n[nodeName];
	var node = nodesArray[nodeIndex];

	// don't do anything if we were already enabled
	if (node.active==true) { return; }

	// find the previous active node
	var previousNodeIndex = nodeIndex-1;
	while( !nodesArray[previousNodeIndex].active ) {
		previousNodeIndex--;
	}
	var previousNode = nodesArray[previousNodeIndex];

	// get the next node in the chain
	var nextNode = previousNode.nextActiveNode;

	// do the wiring
	node.connect(nextNode);					// me -> next
	previousNode.disconnect();				// prev -> null
	previousNode.connect(node);				// prev -> me

	// update fields
	previousNode.nextActiveNode = node;		// prev -> me
	node.nextActiveNode = nextNode;			// me -> next
	
	node.active = true;

	// printNodes();
}

// Node constructor
function Node(mediaElement) {
	this.active = false;
	this.nextActiveNode = undefined;
	this.mediaElement = mediaElement;
	this.connect = function(node) { this.mediaElement.connect(node.getInput()); }
	this.disconnect = function() { this.mediaElement.disconnect(); }
	this.getInput = function() { return this.mediaElement; }
}
