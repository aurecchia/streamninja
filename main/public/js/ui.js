$(function() {     


    // TO REMOVE AFTER TESTING
    hostName = "10.62.96.181";

    //dragStart appened or not
    dragEvent = false;

    // global variable for the Audio API context;
    var context = new webkitAudioContext();

    // global variable for player
    // var player = $("#player")[0];
    // player is already a global variable, defined in player.js

    // initialize main layout
    // __________________________________________
    // |________________________________________|
    // |     |                            |     |
    // |     |                            |     |
    // |     |____________________________|     |
    // |     |                            |     |
    // |     |                            |     |
    // |     |                            |     |
    // |     |                            |     |
    // |     |                            |     |
    // |     |                            |     |
    // |     |                            |     |
    // |     |____________________________|     |
    // |_____|____________________________|_____|
    $("body").layout({
        defaults: {
            livePaneResizing: true,
            spacing_open: 5,
            spacing_closed: 5,
            hideTogglerOnSlide: false,
            togglerLength_open: -1,
            togglerLength_closed: -1
        },
        // media library pane
        center: {
            minWidth: 850
        },
        // player pane
        north: {
            closable: false,
            resizable: false,
            size: "50",
            minHeight: 50,
            spacing_open: 0,
            spacing_closed: 0
        },
        // social pane
        west: {
            minSize: 200,
            size: "200"
        },
        // playlist pane
        east: {
            minSize: 200,
            size: "200"
        }
    });

    // Inside media library pane
    var center = $("#centerPane").layout({
        defaults: {
            livePaneResizing: true,
            spacing_open: 5,
            spacing_closed: 5,
            hideTogglerOnSlide: false,
            togglerLength_open: -1,
            togglerLength_closed: -1
        },
        // effects pane
        north: {
            initClosed: true,
            spacing_open: 4,
            spacing_closed: 0,
            minSize: 248,
            resizable: true,
            size: "248"
        },
        // buttons pane
        west: {
            spacing_open: 4,
            spacing_closed: 4,
            closable: false,
            resizable: false,
            size: "40"
        }
    });

    $("#leftPane").layout({
        defaults: {
            livePaneResizing: true,
            spacing_open: 5,
            spacing_closed: 5,
            hideTogglerOnSlide: false,
            togglerLength_open: -1,
            togglerLength_closed: -1
        },
        center: {
            minSize: 200,
        },
        south: {
            minSize: 200,
            size: "200"
        }
    })

    
    // create control buttons
    $( "#upbutton" ).button( { text: true} );
    $( "#fb-button" ).button( { text: true} );
    $( "#expandButton" ).button( {icons: {primary: "ui-icon-minus"}, text: false} );
    $( "#clearButton" ).button( {icons: {primary: "ui-icon-closethick"}, text: true} );
    $( "#newPlaylistButton" ).button( {icons: {primary: "ui-icon-plusthick"}, text: true} );
    $( "#roomButton" ).button( {text: true} );

    // bind toggle for effects pane
    $("#effectsButton").bind("click", function(event) {
            center.toggle("north");
            if($("#effectsButton").button("option", "icons").primary == "ui-icon-arrowstop-1-s"){
                playerUI.setEffectsUpIcon();
                $("#letterSelector li").css("margin-bottom", "2px")
            }
            else{
                playerUI.setEffectsDownIcon();
                $("#letterSelector li").css("margin-bottom", "10px")
            }
    });
    
    // bind filebrowser for upload button
    $("#upbutton").bind("click", function(event) { $("#playerAdditional div input").trigger("click"); });

    $("#expandButton").bind("click", function() {
        if ($("#expandButton").button("option", "icons").primary === "ui-icon-plus") {
            $(".artistBox, .albumBox").next().children().show();
            $(".artistBox, .albumBox").next().slideDown(300);
            $("#expandButton").button("option", "icons", {primary: "ui-icon-minus"});
        } else {
            $("#searchBox").attr("value", "");
            $(".artistBox, .albumBox").next().slideUp(300);
            $(".artistBox, .albumBox").next().children().show();
            $("#searchBox").attr("value", "");   
            $("#expandButton").button("option", "icons", {primary: "ui-icon-plus"});
        }
    })

    // create overlay for drag and drop
    $("body").append("<div id=\"dragOverlay\" class=\"transparent belowEverything\"><div id=\"dragError\" class=\"transparent\">You can only upload audio files!</div><div id=\"dragMessage\">Drop the songs here!</div></div>");

    $("body").bind("dragenter dragleave", function(event) {
        if(!dragEvent) {
            $("#dragOverlay").toggleClass("transparent belowEverything");
        }
    });
    // function to hide and reset the overlay pane 
    var hideOverlayPane = function(tid) {
        clearTimeout(tid);
        $("#dragOverlay").unbind("click");
        $("#dragOverlay").addClass("transparent");
        setTimeout(function() {
            $("#dragError").addClass("transparent");
            $("#dragMessage").removeClass("transparent");
            $("#dragOverlay").addClass("belowEverything");
        }, 300);
    }

    // listener for overlay pane on drop
    $("body").bind("drop", function(event) {
        if(!dragEvent) {
            var files = event.originalEvent.dataTransfer.files;
            for(i = 0; i< files.length; i++) {
                if(!files[i].type.match("audio.*")) {
                    $("#dragError").removeClass("transparent");
                    $("#dragMessage").addClass("transparent");
                    var tid = setTimeout(function() { hideOverlayPane(tid); }, 2000);
                    $("#dragOverlay").bind("click", function() { hideOverlayPane(tid); });	
                    return;
                }
            }
        }
        hideOverlayPane();
    });

    

    $(".albumBox").bind("dragstart", function(e) {
    	dragEvent = true;
    	var data = new Array();
    	$(this).next().find("li").each(function() {data.push($(this).data("data"))});
    	e.originalEvent.dataTransfer.setData('Data', JSON.stringify(data));
    });

    $(".trackBox").bind("dragstart", function(e) {
    	dragEvent = true;
    	var data = [$(this).data("data")];
    	e.originalEvent.dataTransfer.setData('Data', JSON.stringify(data));
    });

});

