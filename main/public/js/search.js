search = new Object();

search.search = function() {
    
    // get search value and replace multiple spaces with one
    var searchString = $("#searchBox").attr("value");
    searchString = searchString.replace(/\s+/g, " ");
    
    if (searchString === " ") {
        // reset search and elements
    }

    // split multiple search terms by spaces
    var searchTerms = searchString.split(" ");

    var found = [];

    // iterate over trackboxes get the attached song elements and perform the search
    $(".trackBox").each(function() {
        var songObj = $(this).data("data");
        for (index in searchTerms) {
            var term = searchTerms[index].toLowerCase();
            if (songObj.title.toLowerCase().indexOf(term) == -1 &&
                songObj.artist.toLowerCase().indexOf(term) == -1 &&
                songObj.album.toLowerCase().indexOf(term) == -1) {
                return;
            }
        }
        found.push($(this));
    });
    
    $(".albumBox, .artistBox").next().slideUp(0);
    $(".trackBox").hide();
    
    for (index in found) {
        found[index].parents().prev(".artistBox, .albumBox").next().slideDown(0);
        found[index].show();
    }

    if( found.length > 0 ) {
        $("#library").scrollTo($(found[0]).parent().parent().parent().prev(), 300, {axis: "y", easing: "easeInOutQuad"}); 
    }
}


search.filterByLetter = function(letter) {
    var first;
    $(".artistBox").each(function() {
        if ($(this).html().charAt(0).toUpperCase() !== letter) {
            $(this).next().slideUp();
        } else {
            if (first === undefined) {
                first = $(this);
            }
            $(this).next().children().children("ul").children().show();
            $(this).next().children().children("ul").show();
            $(this).next().slideDown();
        }
        
    });
    setTimeout(function() {
        $("#library").scrollTo(first, 300, {axis: "y", easing: "easeInOutQuad"}); 
    }, 380);

    $("#searchBox").attr("value", "");
}

$(function() {
    var tid;
    $("#searchBox").bind("keyup", function() {
        clearTimeout(tid);
        tid = setTimeout(search.search, 200);
    });
});
