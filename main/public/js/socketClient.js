var socket;

socketClient = new Object();

socketClient.connectToSocket = function() {
    //connect
    socket = io.connect('http://streamninja.inf.usi.ch:3000');
    
    //login
    socket.emit('login', {"id":getId(), "name": getName()});

    socket.on('newRoom', function(message) {
	// console.log(message);
	if(message == undefined || message.room == undefined || message.room == "") return;
	    //create the room
		var room = $("<li id=\"room "+message.room+"\" class=\"room\" onclick=\"leftPane.joinRoom('"+message.room+"')\"><div class=\"roomBox\">"+message.room+"</div><ul></ul></li>");
		$("#roomsList").append(room);
    });

    
    //get the list of all users in the chat room
    socket.on('addUsers', function(message) {
	    // console.log(message);
		if(message == undefined || message.users == undefined || message.room == undefined) return;
		var users = JSON.parse(message.users);
		//var room = $("#"+message.room).find("ul");
	    var room = document.getElementById("room "+message.room);
	    room = $(room).find("ul");
		for(var i = 0; i < users.length; i++) {
		    var user = $("<li id=\""+users[i].id+"\" class=\"user\">"+users[i].name+"</li>");
		    if(users[i].master) user.addClass("admin");
		    room.append(user);
            leftPane.attachUsersMenu(user);
        }
    });
    
    //add a single user to the chat room display
    socket.on('addUser', function(message) {
        console.log(message);
        if(message == undefined || message.id == undefined || message.name == undefined || message.room == undefined) return;
    //		var room = $("#"+message.room).find("ul");	
            console.log(message.room);
        var room = document.getElementById("room "+message.room);
        room = $(room).find("ul");
        var user = $("<li id=\""+message.id+"\"class=\"user\">"+message.name+"</li>");
        if(message.master) {
            user.addClass("admin");
        } else {
            leftPane.attachUsersMenu(user);
        }
        room.append(user);
    });
    
    socket.on('removeUser', function(message) {
	if(message == undefined || message.id == undefined  || message.name == undefined || message.room == undefined) return;
		$("#"+message.id).remove();
    });

    socket.on('removeRoom', function(message) {
		if(message==undefined || message.room == undefined) return
//		$("#"+message.room).remove();
	var room = document.getElementById("room "+message.room);
	room = $(room).remove();
    });
    
    socket.on('chatMessage', function(message) {
		if(message.id == undefined || message.name == undefined || message.message == undefined) return;	
		var chatMessage = $("<li class=\"chatMessage\" >"+"<span class=\"chatUser\">"+message.name+ "</span> " + message.message+"</li>");
		$("#chatMessagesList").append(chatMessage);
        $("#chatContainer").scrollTop($("#chatContainer")[0].scrollHeight);
	});
    
    socket.on('addAdmin', function(message) {
	    var room = document.getElementById("room "+message.room);
	    room = $(room).find("#"+message.id).addClass("admin");
    });

    socket.on('player', function(message) {
    	console.log('socket.on("player") received:'+JSON.stringify(message));
	    if (message == undefined) { console.error("socket on player received nothing"); return; }
    	if (message.currentSong) player.play(message.currentSong, true);
    	if (message.nextSong) player.loadNextSong(message.currentSong, true);
    	if (message.data) playerUI.setPlayerInfo(message.data);
    	if (message.songBegin) {
    	    var elapsed = Date.now() - message.songBegin;
    	    console.log("elapsed="+elapsed);
    	    if (elapsed > 0) {	// meaning that the song started already, keep up!
    		    var delay = elapsed/1000; // seconds
    		    console.log(delay);
    		    player.seeking(message.time, true);
    	    } else {
    		    setTimeout(function() { player.send({currentTime:0, play:true}, true); }, - elapsed);
    	    }
    	}
    	playerUI.updatePlayerUI(message);
    	if (message.play !== undefined)
    	    message.play ? playerUI.setPauseIcon() : playerUI.setPlayIcon();
    	if (!message.currentSong) {	// if we dont change source send player stuff
    	    player.send(message, true);
    	} else {					// otherwise wait a bit for the player to load
    	    setTimeout(function() {player.send(message, true)}, 100);
    	}
    });
    
    socket.on('updateLibrary', function(message) {
	mediaLibrary.displayLibrary();
    });

    socket.on('updatePlaylists', function(message) {
	leftPane.displayUserPlaylist();
	//TODO update current too?
    });
}

//function to send message from other files
socketClient.send = function(type, message) {
    if (socket && !isEmpty(message))
        socket.emit(type, message);
}

var isEmpty = function(obj) {
    for (var i in obj) {
        return false;
	}
	return true;
}
