var preventReaction = function (e) {
    e.stopPropagation();
    e.preventDefault();
}

function sendFile(file, index) {
    var xhr;
    var extension;
    if(!file.type.match('audio.*')) {
        return;
    }
    $("#uploadDialog").removeClass("hidden");
    $("#uploadDialog").removeClass("transparent");
    $("#uploadDialog").append("<div id=\"upload_" + index + "\" class=\"uploadProgress\"></div>");
    $("#upload_" + index).progressbar({ max: 100, min: 0, value: 0 });

    var formData = new FormData();
    formData.append("file",file);
    extension = file.name.substring(file.name.lastIndexOf("."));
    formData.append("extension", extension);

    xhr = new XMLHttpRequest();     
    xhr.open('POST', '/media', true);

    // upload download progressbar
    var evtSource = xhr.upload || xhr;
        
    evtSource.addEventListener("progress", function(e) {
        if (e.lengthComputable) {
            $("#upload_" + index).progressbar("option", "value", Math.round((e.loaded / e.total) * 100));
        }
    }, false);
    
    // check upload state and change progressbar accordingly
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 ) {
            if (xhr.status == 200) {
                $("#upload_" + index + " .ui-progressbar-value").addClass("uploadSuccess");
            } else {
                $("#upload_" + index + " .ui-progressbar-value").addClass("uploadError");
            }
            
            setTimeout(function() { 
                $("#upload_" + index).addClass("transparent");
                setTimeout(function() {
                    $("#upload_" + index).slideUp(500); 
                }, 200);
            }, 1500);
            setTimeout(function() { 
                $("#upload_" + index).remove()
                if ($("#uploadDialog div").length == 0) {
                    setTimeout(function() { $("#uploadDialog").addClass("transparent") }, 500);
                    setTimeout(function() { $("#uploadDialog").addClass("hidden") }, 2000);
                }
            }, 2000);
        
            // update library
            displayLibrary();
        }
    }
    xhr.send(formData);
}

$(function() {
    // bind events for drag upload 
    $("body").bind("dragenter dragexit dragover", preventReaction);
    $("body").bind("drop", function(e){
        e.stopPropagation();
        e.preventDefault();
        if (!dragEvent) {
            var formData = new FormData();
            var files = e.originalEvent.dataTransfer.files;

            // loop with pause between file uploads
            var i = 0;
            var iid = setInterval(function() {
                sendFile(files[i], i++);
                if (i == files.length) {
                    clearInterval(iid);
                }
            }, 100);
        }
        dragEvent = false;
    });
     
    // bind events for normal file upload
    $("#file").bind('change', function(e) {
        sendFile(e.currentTarget.files[0], 0);
    });
});

