var schemas = require('../models/models.js');
var ids = [];

//get a list of all the playlist. TODO: update it when someone create a new playlist

var rooms = new Object();

exports.init = function(server) {
    var io = require('socket.io').listen(server);
    io.set('log level', 1);
    io.sockets.on('connection', function(socket) {
		//first message, init
		socket.on('login', function(message) {
		    if(message == undefined || message.id == undefined ||  message.name == undefined) return;
		    //not working till we removed logged users.
		    //if(ids.indexOf(message.fb.id) > -1 ) {
		    ids.push(socket.user);
		    //save needed information about the user into his socket
		    socket.user = new Object();
		    socket.user.id = message.id;
		    socket.user.name = message.name;
		    socket.user.room = undefined;
		    //    } else {
		    // TODO refuseConnection
		    //    }
		    for(var room in io.sockets.manager.rooms) {
			socket.emit('newRoom', {"room":room.slice(1, room.length)});
			var list = [];
			for(var i = 0; i < io.sockets.clients(room.slice(1, room.length)).length; i++) {
			    var tosend = JSON.parse(JSON.stringify(io.sockets.clients(room.slice(1, room.length))[i].user));
			    var master = false;
			    if(rooms[room.slice(1, room.length)] != undefined) {
				if(rooms[room.slice(1, room.length)].admins.indexOf(tosend.id) > -1) master = true;
			    }
			    tosend.master = master;
			    list.push(tosend);
			}
			socket.emit('addUsers', {"users": JSON.stringify(list), "room": room.slice(1, room.length)});
		    }
		});
		
		//new Room
		socket.on('newRoom', function(message) {
		    if(message == undefined || message.name == undefined || message.room == undefined || message.id == undefined || message.status == undefined) return;
		    // console.log(socket.user);
		    var id = message.id;
		    rooms[message.room] = message.status;
		    rooms[message.room].admins = [];
		    rooms[message.room].admins.push(id);
		    console.log(rooms[message.room]);
		    // console.log(room[message.room]);
		    
		    if(socket.user!=undefined && socket.user.room != undefined) {
				socket.leave(socket.user.room);	    
				io.sockets.emit('removeUser', {"id":message.id, "name":message.name, "room" :socket.user.room})
				if(io.sockets.manager.rooms["/"+socket.user.room]==undefined) {
				    io.sockets.emit('removeRoom', {"room":socket.user.room});
				    rooms[socket.user.room] = undefined;
				}
		    }
		    socket.join(message.room);
		    if(socket.user == undefined) {
			socket.user = new Object();
			socket.user.id = message.id;
			socket.user.name = message.name;
		    }
		    socket.user.room = message.room;
		    io.sockets.emit('newRoom', {"room":message.room});
		    io.sockets.emit('addUser', {"id":message.id, "name":message.name, "room":message.room, "master":true});	    
		     console.log(io.sockets.manager.rooms);
	            // console.log(message.status);
		});
			
		//message when someone change playlist
	socket.on('changeRoom', function(message) {
		    console.log(message);
		    if(message == undefined || message.id ==undefined || message.name == undefined || message.room == undefined || socket.user == undefined) return;
		    if(message.room == socket.user.room) return;
		    if(io.sockets.manager.rooms["/"+message.room]==undefined) {
				console.log("ROOM DOESN'T EXIST");
				return; //TODO REFUSE	
		    }
		    
		    //leave the room and notify all users about that 
		    if(socket.user.room != undefined) {
				socket.leave(socket.user.room);	    
				io.sockets.emit('removeUser', {"id":message.id, "name":message.name, "room" :socket.user.room});
				if(io.sockets.manager.rooms["/"+socket.user.room]==undefined) {
				    io.sockets.emit('removeRoom', {"room":socket.user.room});
				    rooms[socket.user.room] = undefined;
				}
		    }
		    
		    
		    socket.user.room = message.room;
	    var master = false;
	    if(rooms[message.room].admins.indexOf(message.id) > -1) master = true;
	    io.sockets.emit('addUser', {"id":message.id, "name":message.name, "room":message.room, "master":master });
		    socket.join(message.room);
		    socket.emit('player', rooms[message.room]);
		});

		socket.on('chatMessage', function(message) {
		    if(message.id == undefined || message.name == undefined || message.message == undefined) return;
		    if(socket.user == undefined || socket.user.room == undefined) return
		    io.sockets.in(socket.user.room).emit('chatMessage', {"id": message.id, "name":message.name, "message": message.message});
		    
		});

		socket.on('player', function(message) {
		    if (message == undefined) return;

		    console.log("socketServer received message on player: "+JSON.stringify(message));
		    if (isEmpty(message)) return;
		    
		    // merge message with previous room status
		    if (!rooms[socket.user.room]) rooms[socket.user.room] = new Object();
		    merge(rooms[socket.user.room], message);
		    
		    // broadcast message
		    socket.broadcast.to(socket.user.room).emit("player", message);
		});
	
	socket.on('addAdmin', function(message) {
	    if(message== undefined || message.id == undefined || message.room == undefined || rooms[message.room] == undefined) return
	    rooms[message.room].admins.push(message.id);
	    io.sockets.emit('addAdmin', {'id': message.id, 'room': message.room});
	});

	socket.on('updatePlaylists', function(message) {
	    io.sockets.emit('updatePlaylists', {});
	});

	socket.on('updateLibrary', function(message) {
	    io.sockets.emit('updateLibrary', {});
	});
	
	socket.on('disconnect', function(message) {
	    if(socket.user == undefined) return;
	    if(socket.user && socket.user.room != undefined) {
		io.sockets.emit('removeUser', {"id":socket.user.id, "name":socket.user.name, "room" :socket.user.room});
		if(io.sockets.manager.rooms["/"+socket.user.room]==undefined) {
		    io.sockets.emit('removeRoom', {"room":socket.user.room});
		    rooms[socket.user.room] = undefined;
		}
	    }
	});
    });
}

var isEmpty = function(obj) {
	for (var i in obj) {
		return false;
	}
	return true;
}

// merges object 2 into obj1
var merge = function(obj1, obj2) {
    for (var key in obj2) {
        if (key != "volume") {
            if (typeof(obj2[key])!=="object") {
                // put in obj1 every non-object field of obj2
                obj1[key] = obj2[key];
            } else {
                if (obj1[key] == undefined) {
                    // put in obj1 every object field of obj2 which does not exist in obj1
                    obj1[key] = obj2[key];
                } else {
                    // or merge the inner object recursively
                    merge(obj1[key], obj2[key]);
                }
            }
        }
    }
}
