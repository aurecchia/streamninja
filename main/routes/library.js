var schemas = require('../models/models.js');//import schemas from models.js
var fs = require("fs");//filesystem module
var taglib = require("taglib");
var db = require('../app.js').db;

exports.upload = function(req, res){ //post on /media adds entries to database and uploads songs to /media folder

    var extension = req.body.extension;

    var fp = (req.files.file.path).substring(req.files.file.path.lastIndexOf("/")+1);//filepath without the unneeded superpath

    var filename = req.files.file.name.substring(0, req.files.file.name.lastIndexOf("."));//filename

    var finalName = req.files.file.path+extension;

    //adding correct extension to the file in /media
    fs.rename(req.files.file.path, finalName, function(err) {
        if(!err){
            // taglib
            taglib.read(finalName, function(err, tags, audioProperties) {
                if(!err){


                  //Schema models
                    var Song = db.model("song", schemas.Song);
                    var Album = db.model("album", schemas.Album);
                    var Artist = db.model("artists", schemas.Artist);
                    var Library = db.model("library", schemas.Library);

                    //Schemas get mp3 tags if any, unknown fields otherwise
                    var newSong = new Song({
                        title: tags.title || filename,
                        artist: tags.artist || "Unknown Artist",
                        album: tags.album || "Unknown Album",
                        track: tags.track || "0",
                        genre: tags.genre || "Unknown Genre",
                        path: fp+extension,
                        duration: audioProperties.length
                    });
                    
                    var newAlbum = new Album({
                        title: tags.album || "Unknown Album",
                        artist: tags.artist || "Unknown Artist",
                        songs: newSong || filename 
                    });

                    var newArt = new Artist({
                        name: tags.artist || "Unknown Artist",
                        albums: newAlbum || "Unknown Album"
                    });
                    
                    var newLib = new Library({
                        artists: newArt || []
                    });

                    Library.find({"artists.name": { $in: [newSong.artist]}}, function(err, found){ //Checking wether the artist is already in the db or not.
                        found = found || [];
//                        console.log("Looking For Artists: "+newSong.artist);
//                        console.log(found);
                        if(found.length == 0){//not found, adding it
                            newLib.save();
                            res.send(200, newSong);
                            console.log(newArt.name+" successfully added to db.libraries.artists.");
                        }
                        else{//hierarchically descend db tree for albums/songs and adding where they should.
                            newLib = found[0];
                            Library.find({"artists.albums.title": { $in: [newSong.album]}}, function(err, found){
//                                console.log("Looking For Album: "+newSong.album);
                                if(found.length == 0){
                                    newLib.artists[0].albums.push(newAlbum);
                                    newLib.save();
                                    res.send(200, newSong);
                                    console.log(newAlbum.title+" successfully added to db.libraries.artists.albums.");
                                }
                                else{
//                                    console.log(found[0].artists[0].albums);
                                    Library.find({"artists.albums.songs.title": { $in: [newSong.title]}}, function(err, found){
//                                        console.log("Looking For Song: "+newSong.title);
//                                        console.log(found);
                                        if(found.length == 0){
                                            for(var i = 0; i < newLib.artists[0].albums.length; i++){
                                                if(newLib.artists[0].albums[i].title === newSong.album){
                                                    newLib.artists[0].albums[i].songs.push(newSong);
                                            }
                                        }
                                        newLib.save();
                                        res.send(200, newSong);
                                        console.log(newSong.title+" successfully added to db.libraries.artists.albums.songs.")
                                        }
                                        else{//The entry is already in the database, conflict
                                            console.log("The song is already on the server. Aborting..");
                                            fs.unlink(req.files.file.path+tags.extension); 
                                            res.send(409, "File already on server/database.");
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
                else{
                    console.log("Error!");
                    console.log(err);
                }
            });
        }
        else{
            console.log("Rename Error!");
            console.log(err);
        }
    });
};

exports.removeSong = function(req, res){//remove songs from media library, both database and hdd.
    
    var songName = JSON.parse(req.body.songName);
    var Song = db.model("song", schemas.Song);
    var Library = db.model("library", schemas.Library);
    var newLib = new Library();
    var toRemove = new Song();
    Library.find({"artists.albums.songs.title": {$in: [songName.title]}}, function(err, found){//find the song requested
        console.log("Looking for "+songName.title+" to remove in libraries.libraries");
        console.log("Found: ");
        if(found.length == 0){
            res.send(404, "Song "+songName+" does not seem to exist. How the hell is that even possible...?");
        }
        else{
            console.log(found[0].artists[0].albums);
            for(var j = 0; j < found[0].artists[0].albums.length; j++){//look for the right album
                for(var i = 0; i < found[0].artists[0].albums[j].songs.length; i++){//which constains it.
                    if(found[0].artists[0].albums[j].songs[i].title === songName.title){//found it..?
                        toRemove = found[0].artists[0].albums[j].songs[i];//this is it!
                        console.log(toRemove.title+" BEING POPPED OUT!");
                        var popped = toRemove.remove();
                        //console.log(popped.title);
                        //console.log("is what should have been removed.");
                        found[0].save(function(err){
                            if(!err){
                                console.log("Will now check if there are empty entries..");//Was it the last song in the album?
                                Library.find({"artists.name": { $in: [songName.artist]}}, function(err, found){ 
                                    newLib = found[0];
                                    console.log("Artist being checked:");
                                    console.log(newLib.artists[0].name+" with "+newLib.artists[0].albums.length+" albums.");
                                    for(var i = 0; i < newLib.artists[0].albums.length; i++){// Let's see!
                                        console.log("Checking length of: "+newLib.artists[0].albums[i].title);
                                        console.log("Which is.. "+newLib.artists[0].albums[i].songs.length);
                                        if(newLib.artists[0].albums[i].songs.length === 0){//Yep, it was.
                                            console.log("The albums has no songs! Removing..");
                                            console.log("Removing empty shiz: "+newLib.artists[0].albums[i].title);
                                            //newLib.artists[0].albums.pop({"title": newLib.artists[0].albums[i].title});
                                            popped = newLib.artists[0].albums[i].remove();//pwning the album because it was empty.
                                            console.log(popped.title);
                                            console.log("is what should have been removed.");
                                            newLib.save(function(err){
                                                if(newLib.artists[0].albums.length === 0){//Was it the last album of this artist?
                                                    console.log("The artist had no albums! Removing..");
                                                    console.log("Removing empty shiz: "+newLib.artists[0].name);
                                                    //newLib.artists.pop({"name": newLib.artists[0].name});
                                                    popped = newLib.remove();//Yeah, removing.
                                                    console.log("is what should have been removed.");
                                                    newLib.save(function(err){
                                                        if(!err){//All good
                                                            console.log("Collection saved with no errors.");
                                                            res.send(200, "Ok dropped collection");
                                                        }
                                                    });
                                                }
                                                res.send(200, "Ok, artist/album removed.")
                                           });
                                        }  
                                    }
                                });
                            }
                            else{
                                console.log("Shit Happened YO");
                            }
                            console.log(songName.title+" successfully removed from library.");
                        });
                    }
                }            
            }
           fs.unlink("/home/streamninja/streamninjaServer/streamninja/main/public/media/"+songName.path, function(err){//remove the file from public/media
               if(err){
                    throw err;
                    console.log("No such file found, or not enough permissions.")
               }
           });
        }
    });
    res.send(200, "Ok, song removed.");
}

exports.getDatabase = function(req, res){//get array of artists with a get on /library, used for UI creation
    var Library = db.model("library", schemas.Library);
    Library.find("", function(err, found){
            res.send(found);
    });
}

exports.createPlaylist = function(req, res) {//creates playlist entry in DB with post on /playlist/:id; called from the create playlist button
    var token = req.body.token;// facebook login token
    var type = req.body.type;
    var Playlist = db.model("playlist", schemas.Playlist);
    Playlist.find({"name":req.params.id, "owner": token}, function(err, found) {//Is there already a playlist with the same name and owner?
        if(found.length == 0) {//Nope, can create it.
            var newPlaylist = new Playlist({
                name: req.params.id,
                owner: token,
        		type: type,
                songs: []
            });
            newPlaylist.save(); //Done
            res.send(200, "OK, playlist created");
        }
        else {//There already is one!
            res.send(409, "Such playlist already exists");
        }
    });
}

exports.getPlaylistById = function(req, res) {//gets playlist with name: id. NO SHIT SHERLOCK
    var owner = req.params.token;
    var Playlist = db.model("playlist", schemas.Playlist);
    Playlist.find({"name": req.params.id, "owner": owner}, function(err, found) {//look for it
        if(found.length != 0){//there it is
    	    res.send(200, found);
        }
        else{
            res.send(404, "Playlist with such owner does not exist.");
        }
    });
}

exports.getFriendsPlaylistByOwner = function(req, res) {//gets playlists of all friends of the users
    var friends = JSON.parse(req.body.friends);//array of fb login tokens
    var c = 0;//counter
    var listOfPlaylists = {};
    var Playlist = db.model("playlist", schemas.Playlist);
    for(var i = 0; i < friends.length; i++){
        Playlist.find({"owner": friends[i].id}, function(err, found) {//looking for playlist for each token
            if(found.length != 0){
                listOfPlaylists[JSON.stringify(friends[c])] = found; //adding them to the object
	    }
            c++;
            if(c === friends.length){//needed to know when to exit
                console.log(listOfPlaylists);
		res.send(200, listOfPlaylists);        
            }
        });
    }
}

exports.getPlaylistByOwner = function(req, res){//get playlist by owner :id
    var Playlist = db.model("playlist", schemas.Playlist);
    Playlist.find({"owner": req.params.id}, function(err, found){
        if(found.length != 0){// > 1 found
            res.send(200, found);//sending
        }
        else{
            res.send(404, "No playlists bla");
        }
    });
}


exports.editPlaylist = function(req, res){//modify playlist with put on /playlist/:id, called when adragging songs from library to playlist sidebar
    var Playlist = db.model("playlist", schemas.Playlist); 
    var Library = db.model("library", schemas.Library);

    var Song = db.model("song", schemas.Song);

    var toEdit = new Playlist();
    var toAdd = new Song();
    var songName = JSON.parse(req.body.songName);

    var token = req.body.token;
    var newName = req.body.newId;
    //Branch in which edit playlist = change name 
    if (newName !== undefined) {//passed a valid string for new name
        Playlist.find({"name":req.params.id, "owner": token}, function(err, found) {//Finding the PL to edit..
            if (found.length != 1) {//Ow, the playlist didnt exist or there were more than 1. (Impossible)
                console.log("Something went horribly bad!") // this should never ever happen
                console.log(found)
                res.send(400, "Playlist to edit was not found or was found multiple times!");
            } else if (found[0].owner != token || newName == found[0].name) {//User requesting edit was not the owner of the pl, or the name given is already in the db
                console.log("User cannot edit this playlist because he sucks, or there is already a playlist with this name.");
                console.log(found[0].owner);
                console.log(token)
                res.send(400, "Playlist to edit was not owned by user or already exists!");
            } else {//Good to go
                console.log("> editing")
                found[0].name = newName;
                found[0].save();
                console.log(found[0]);
                res.send(200, "Ok, playlist name changed")
            } 
        });
    }
    else{//Branch in which edit playlist = add songs
        Library.find({"artists.albums.songs.title": { $in: [songName]}}, function(err, found){//first thing retrieve file tags from db
            if(found.length == 0){
                res.send(404, "Song "+songName+" does not seem to exist. How the hell is that even possible..?");//this should never happen
            }
            else{
               for(var j = 0; j < found[0].artists[0].albums.length; j++){
                   for(var i = 0; i < found[0].artists[0].albums[j].songs.length; i++){
                       if(found[0].artists[0].albums[j].songs[i].title === songName){
                            toAdd = found[0].artists[0].albums[j].songs[i];//found song to add to pl
                       }
                   }
               }
               console.log("Adding song..");
               console.log(toAdd);
               Playlist.find({"name":req.params.id, "owner": token}, function(err, found){//then look on database for the playlist to add song in
//                    console.log("Looking for playlist "+req.params.id+" into libraries.playlists"); 
                    if(found.length == 0){//this should not ever happen either
                        res.send(404, "Playlist "+req.params.id+" does not seem to exist. How thei hell is that even possible..?");
                    }          
                    else if(found[0].owner != token){//Simple user trying to change playlist
                        res.send(400, token+" is not the owner of the playlist, which is owned by: "+found[0].owner); 
                    }
                    else{//regular flow, adding song to the playlist collection
                        toEdit = found[0];
                        toEdit.songs.push(toAdd);
                        toEdit.save();
                        console.log(toAdd+" correctly added to "+toEdit.name); 
                        res.send(200, "Ok, song added");
                    }   
               });
            }   
       });
    }
 }


exports.deletePlaylist = function(req, res){//just removing playlist
    var playlist = req.params.id;
    var owner = req.params.token;
    var Playlist = db.model("playlist", schemas.Playlist); 
    var toRemove = new Playlist(); 
    Playlist.find({"name" : playlist, "owner": owner}, function(err, found){//check if the playlist exists (should always exist)
//        console.log("Looking for playlist "+playlist+" in libraries.playlists.");
//        console.log(found);
        if(found.length == 0){//not supposed to happen. EVER
            res.send(404, "Playlist "+req.params.id+" does not seem to exist. How thei hell is that even possible..?");
        }
        else{//going for the boom
            for(var i = 0; i < found.length; i++){
                if(found[i].name == playlist && found[i].owner == owner){
                    toRemove = found[i];
                    toRemove.remove();//kthxbai
                }
            }
            console.log("Playlist "+toRemove.name+" successfully removed!");
            res.send(200, "Ok, playlist removed");//yay
        }
    });
}

exports.removeSongFromPL = function(req, res){//del song from playlist with name :id
    var songs = JSON.parse(req.body.songName);
    var playlist = req.params.id;
    var owner = req.body.token;
    var Playlist = db.model("playlist", schemas.Playlist);
    var toEdit = new Playlist();
    Playlist.find({"name" : playlist, "owner": owner}, function(err, found){//check if the playlist exists (should always exist)
//        console.log("Looking for playlist "+playlist+" in libraries.playlists.");
//        console.log("Found: ");
//        console.log(found);
        if(found.length == 0){//not supposed to happen. EVER
            res.send(404, "Playlist "+req.params.id+" does not seem to exist. How thei hell is that even possible..?");
        }
        else if(found[0].owner != owner){//playlist exists but the user is a faggot
            res.send(400, owner+" is not the owner of the playlist, which is owned by: "+found[0].owner); 
        }
        else{//correct flow
            console.log("playlist found etc");
            console.log("songs.l = "+songs.length+", pl.songs.l = "+found[0].songs.length);
            for(var i = 0; i < songs.length; i++) {
                for(var j = 0; j < found[0].songs.length; j++){
                    if(songs[i] === found[0].songs[j].title){
                        console.log(found[0].songs[j].title+" removed from the playlist "+found[0].name);
                        found[0].songs[j].remove(); 
                        found[0].save(); 
                    }
                }
           }
           res.send(200, "Ok, song removed");
        }
    });
}

exports.editSongID3 = function(req, res){//Used to edit songs title only
    var songName = req.body.songName;//get song to edit

    var field = req.body.field;//which part of it to edit
    var toAdd = req.body.content ;// new content

    var Song = db.model("song", schemas.Song);
    var Album = db.model("album", schemas.Album);
    var Library = db.model("library", schemas.Library);

    var toEdit = new Song();
    var newLib = new Library();
    Library.find({"artists.albums.songs.title": { $in: [songName]}}, function(err, found){//look for it
        console.log("Looking for "+songName+" in libraries.libraries.");
        if(found.length == 0){//herp derp
            res.send(404, "Song "+songName+" does not seem to exist. How the hell is that even possible..?");//this should never happen
        }
        else{//all fine
           newLib = found[0];
           for(var j = 0; j < newLib.artists[0].albums.length; j++){//go through albums
              for(var i = 0; i < newLib.artists[0].albums[j].songs.length; i++){//and each song of each album
                    if(newLib.artists[0].albums[j].songs[i].title === songName){//there we go
                        newLib.artists[0].albums[j].songs[i].title = toAdd; //found the song
                        var filename = "public/media/"+newLib.artists[0].albums[j].songs[i].path;// needed to change file ID3 tags
                    }
              }
           }
           taglib.tag(filename, function(err, tags) {
                if(!err){//magic happens here;w
                    tags.title = toAdd;
                    tags.save(function(err){
                        if(!err){//at this point everything will be changed..
                            newLib.save();
                            console.log("Changed title field for song "+songName+" to "+tags.title);
                            res.send(200, "ID3 successfully changed");
                        }//.. both on disk and db.
                        else{
                            console.log("SHIT HAPPENED YO: "+err);
                            res.send(409, "Something went wrong. See console for more details");
                        }
                    });
                }
                else{
                    console.log("Error!");
                    console.log(err);
                    res.send(409, "Something went wrong. See console for more details");
                }
           });
        }
    });
}

